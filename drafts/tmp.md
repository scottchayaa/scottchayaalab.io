

https://www.ithome.com.tw/news/105366
[DevOps, SRE]
開發流程的程式碼審核（code review）、測試及快速回復（rollback）工具的問題。她解釋，這些程序應該要有能力阻止開發者上傳錯誤的程式碼，不該讓人為疏失蔓延到正式環境中。
