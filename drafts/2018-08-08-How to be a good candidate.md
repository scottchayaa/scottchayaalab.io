---
title: 'How to be a good PHP candidate'
date: 2018-08-08T14:43:10+08:00
image: ["/post/2018/08/08/08-1.png"]
description: 好的求職者應該注意的事項，以及事前準備的技能
tags: ["job", "experience"]
---

### PHP
1. 什麼是composer？有什麼作用？工作原理？

2. 如何實現自動加載？不用composer如何實現？ PSR-4是什麼？

3. PHP如何實現靜態化

4. 你了解設計模式嗎？說下你最常用的設計模式  
> - [PHP設計模式](http://kejyun.github.io/design-pattern-for-php/)
> - [PHP设计模式范例](http://designpatternsphp.readthedocs.io/zh_CN/latest/README.html)

5. 觀察者模式是如何實現的？工廠模式是如何實現的？適配器模式是如何實現的？

6. 解釋一下session和cookie

7. 如何實現不基於session和cookie的用戶認證

8. 說下你目前框架所用到的核心概念

9. 什麼是CSRF攻擊，XSS攻擊？如何防範

10. 什麼是RESTful API？

11. 在程序的開發中，如何提高程序的運行效率？
>（1）優化SQL語句，查詢語句中盡量不使用select *，用哪個字段查哪個字段；少用子查詢可用表連接代替；少用模糊查詢。  
（2）數據表中創建索引。  
（3）對程序中經常用到的數據生成緩存（比如使用redis緩存數據，比如使用ob進行動態頁面靜態化等等）。  
（4）對mysql做主從復制，讀寫分離。 （提高mysq執行效率和查詢速度）  
（5）使用nginx做負載均衡。 （將訪問壓力平均分配到多態服務器）

### Database
1. 你知道nosql嗎？你用的nosql都有哪些？

2. 如何優化查詢？

3. mysql的讀寫分離

4. 消息隊列如何實現

5. redis和memcache有什麼區別

6. 索引有哪些，你是如何做索引的？

7. mysql如何分表
> - [mysql分表的3种方法](http://blog.51yip.com/mysql/949.html)
> - [MySql从一窍不通到入门（五）Sharding：分表、分库、分片和分区](https://blog.csdn.net/KingCat666/article/details/78324678)

8. 對於關係型數據庫而言，索引是相當重要的概念，請回答有關索引的幾個問題：
> a)、索引的目的是什麼？  
快速訪問數據表中的特定信息，提高檢索速度  
創建唯一性索引，保證數據庫表中每一行數據的唯一性。  
加速表和表之間的連接  
使用分組和排序子句進行數據檢索時，可以顯著減少查詢中分組和排序的時間    
b)、索引對數據庫系統的負面影響是什麼？  
負面影響：  
創建索引和維護索引需要耗費時間，這個時間隨著數據量的增加而增加；索引需要佔用物理空間，不光是表需要佔用數據空間，每個索引也需要佔用物理空間；當對錶進行增、刪、改、的時候索引也要動態維護，這樣就降低了數據的維護速度。  
c)、為數據表建立索引的原則有哪些？  
在最頻繁使用的、用以縮小查詢範圍的字段上建立索引。  
在頻繁使用的、需要排序的字段上建立索引  
d)、 什麼情況下不宜建立索引？  
對於查詢中很少涉及的列或者重複值比較多的列，不宜建立索引。  
對於一些特殊的數據類型，不宜建立索引，比如文本字段（text）等。  

### Front-end
1. DOM事件流
2. JS是如何實現繼承的
3. 有使用那些前端框架？ex: Vue.js
4. CSS, Bootstrp, Sass

### Backend-end
1. TCP傳輸控制協定
2. UDP
3. 什麼是socket
4. Authentication : OAuth2.0, JWT

### Security
1. API安全機制
2. AES, RSA...等加密機制說明
3. 網站有那些攻擊方式?

### DevOps
1. Linux

2. AWS

3. GCP

4. Docker

5. CI/CD
> - GitLab CI
> - Jenkins
> - AWS CodePipeLine

6. Unit test注意事項

7. 對於大流量網站，採用什麼方法來解決訪問量的問題
> 確認服務器硬件是否能夠支持當前的流量  
數據庫讀寫分離，優化數據表  
程序功能規則，禁止外部的盜鏈  
控制大文件的下載  
使用不同主機分流主要流量  

### Other
1. 了解你之前專案中碰到的問題？如何解決問題
2. 你未來規劃是什麼？
3. 為什麼我們要僱用你？你能為公司做什麼？
4. 說明你之前做得最好的工作項目？使用那些技術？帶給你或團隊那些成長？

### Reference
 - [三年PHP面试总结](http://ukagaka.github.io/php/2017/08/07/17interview.html)
 - 