

### [10 signs of a mature development pipeline](https://blog.softwaremill.com/10-signs-of-a-mature-development-pipeline-ee1ba1a2fa8b)

1. 使用 Git 當 Version Control
2. 擁有 Continuous Integration 伺服器
3. 擁有多個環境, e.g. Dev, Staging, Production
4. 開發人員可以一鍵部署最新版本到 Production
5. 可以監測應用程式在 Production 環境的行為
6. 設定好 Alert 用以得知使用者遇到問題
7. 擁有收集 Log 的中央管理機制
8. 可以在數秒內 Rollback 改變
9. 可以在數秒內部署 Hot Fix
10. 不重新部署下，更改執行中程式組態的能力