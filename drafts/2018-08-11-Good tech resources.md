---
title: '一些不錯的技術資源'
date: 2018-08-11T14:43:10+08:00
image: ["/post/2018/08/11/11-1.png"]
description: 記錄著平常找資料時，發現的優質技術blog, open sources
tags: ["experience"]
---

# Blog
1. [超猴崽工作日誌](http://g23988.blogspot.tw/)  
起因關注 : 【PHP Laravel】Laravel 5.4 依照不同環境提升至 SSL https
2. https://rickhw.github.io/  
發現一個AWS架構師的github blog
3. [laravel源码解析](https://laravel-china.org/leoyang)  
laravel packagist 程式碼解析專家
4. [Seaony](https://laravel-china.org/users/17036)  
起因關注 : [Eloquent 之 Fill 方法源码解析](https://laravel-china.org/topics/6192/eloquent-fill-method-source-code-analysis)
5. [小惡魔 – 電腦技術 – 工作筆記 – AppleBOY](https://blog.wu-boy.com/)  
起因關注 : [推 - 運用 Docker 整合 Laravel 提升團隊開發效率](https://www.youtube.com/watch?v=CoGfZbGfYI8),[用 Drone CI/CD 整合 Packer 自動產生 GCP 或 AWS 映像檔](https://blog.wu-boy.com/2018/07/drone-with-hashicorp-packer/)
6. [Jimmy Song - 宋净超的博客](https://jimmysong.io/) : 中國K8S專家
7. [小谈博客 - 一个专注 WEB 技术的博客](https://blog.tanteng.me/) : laravel
8. [leoyang - laravel源碼解析blog](https://laravel-china.org/leoyang)

# Github
1. [phper2013/laravel-sms](https://github.com/phper2013/laravel-sms)  
基于 laravel 5.3+ 开发的轻量化手机短信服务包，特点：简单，灵活
2. [PanJiaChen/vue-element-admin](https://github.com/PanJiaChen/vue-element-admin)  
`vue-element-admin` is a front-end management background integration solution. It based on vue and use the UI Toolkit element.

# PPT
1. [Docker進階討論](https://www.slideshare.net/ssusercab70d/docker-73527498)
2. [Docker —— 从入门到实践](https://www.kancloud.cn/docker_practice/docker_practice/469844)
3. [Docker：Swarm + Stack 一站式部署容器集群](https://blog.csdn.net/github_38705794/article/details/77541371)
有示範如何指定replicias分配到所屬的角色, 使用`placement: constraints:`

# DevOps
1. [DevOps 公众号精华汇总和话题征集](https://mp.weixin.qq.com/s/V2sqpb6Ydu52wr4bfhlcig)

# VSCode
1. [小克的 Visual Studio Code 必裝擴充套件（Extensions）私藏推薦](http://goodjack.blogspot.com/2018/03/visual-studio-code-extensions.html)

# Laravel 
1. [NOVA - laravel official admin panel](https://nova.laravel.com/docs/1.0/installation.html#authorizing-nova)
2. [Akaunting - free open source and online accounting software](https://github.com/akaunting/akaunting)
3. [两个非常棒的 Laravel 权限管理包推荐](https://laravel-china.org/articles/5662/two-great-laravel-rights-management-packages-recommended)