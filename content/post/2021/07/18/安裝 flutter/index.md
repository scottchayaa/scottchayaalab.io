---
title: 'Flutter 安裝'
date: 2021-07-18T16:00:10+08:00
image: "/img/default/flutter.png"
description: Flutter 是一個跨平台開發框架，你可以使用一種程式語言(Dart)完成 ios 和 Android APP 開發，也另有支援 Web 和桌面應用程式開發。 以前為了要寫 APP 還要分別學 java 和 object-c ，現在只需要學一種語言就可以做 APP 開發，大幅降低開發和維護成本，老闆也知道開發 APP 是很貴的。相信以後這種跨平台開發元件越來越成熟後，工程師將可以投注更多心力在與 PM 討論核心產品商業邏輯。
tags: ["flutter", "android studio"]
---


# 1. Install Flutter (Windows)

https://flutter.dev/docs/get-started/install

```sh
cd c/
mkdir src/
git clone https://github.com/flutter/flutter.git -b stable
```

> 建議就是使用 git clone 方式下載 flutter 

# 更新 Windows 環境變數

本機 > 內容 > 關於 > 進階系統設定 > 環境變數

編輯 > 新增下面路徑
```
C:\src\flutter\bin
```

驗證是否成功

```sh
$ where flutter dart
C:\src\flutter\bin\flutter
C:\src\flutter\bin\flutter.bat
C:\src\flutter\bin\dart
C:\src\flutter\bin\dart.bat
```

# Run flutter doctor

輸入 `flutter doctor` > 系統會自動去檢查環境

解決遇到的問題 > 想辦法讓他變成`全部綠勾`就對了

![](flutter-doctor.png)




# Install android studio

[Download Android Studio](https://developer.android.com/studio?gclid=CjwKCAjw3MSHBhB3EiwAxcaEu1_X0jpKkrsJj-tXFpA-_yIlMxGE_C2Tf1n45TOPUudrf38uuzOqsRoCMxcQAvD_BwE&gclsrc=aw.ds)

```
// 安裝過程工出現錯誤

Intel® HAXM installation failed. To install Intel® HAXM follow the instructions found at: https://github.com/intel/haxm/wiki/Installation-Instructions-on-Windows

// 有關虛擬化技術的錯誤，可以先忽略之後再解
```

![](2021-07-16-15-43-05.png)


建立一個空白專案 
因為是第一次啟動專案
背景程式會開始跑很多要裝的sdk, 工具 ... (須等一陣子)

![](2021-07-16-15-50-19.png)


# 2. Set up VSCode with Flutter 

https://flutter.dev/docs/get-started/editor?tab=vscode

照指示安裝設定...

安裝 Flutter plugin 後執行  
View > Command Palette > and select the `Flutter: Run Flutter Doctor`  
會出現 `X Visual Studio 2019 or later is required.` 的錯誤  

需要安裝 Visual Studio 2019 + 勾選`使用 C++ 的桌面開發`  

![](flutter-C++-workload.png)



# 3. Test drive

 - 參考 : https://flutter.dev/docs/get-started/test-drive?tab=vscode
 - View > Command Palette > select the `Flutter: New Application Project`
 - 跳出選擇要在哪建立專案目錄
 - 輸入專案名稱: `myapp`, and press Enter.
 - 等待專案建置一段時間後， 會出現 `main.dart` 檔案

## Run App

 - 右下方可以選擇要執行的模擬器
 - 選擇剛在 Android Studio > Tools > AVD Manager 建立好的 `Pixel 4 API 30 ` 模擬器
 - 記得要打開模擬器的`電源按鈕`，不然測試程式時會連不到
 - 按 `F5` 可以直接進入偵錯模式，同時也可以做 Hot Reload
 - 直接修改 `main.dart` 檔案裡的文字試試，畫面會直接套用修改結果

![](test-drive-1.png)

![](pixel4-simulator.png)



# 切換 Flutter 版本

現在直接下載官網的 Flutter stable

你會發現在 `v2` 版本

但剛好你手上維護的專案是 `v1` 時該怎麼辦

 - 到你剛剛 git clone 下來的 flutter 目錄
 - cd /c/src/flutter
 - 查看目前有的 tag 版本 : `git tag`
 - git checkout 1.22.6
 - git branch Branch_1.22.6
 - git checkout Branch_1.22.6
 - flutter --version

這樣就 flutter 就切換版本完成了

