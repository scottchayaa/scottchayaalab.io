---
title: 'AWS EFS 功能介紹'
date: 2018-12-01T13:30:10+08:00
image: "/post/2018/12/01/cover.jpg"
description: 最近在讀AWS Certification相關資料，關於儲存工具方面了解不多(其實發現還有很多不會)，這邊先挑一個比較好入門解釋的EFS，大家在公司一定會常常使用到類似的服務叫做『共用硬碟』，也有人叫『公槽』。當然AWS提供的公槽服務不會只有存放資料這麼簡單，設計架構上可以容納千人等級的並行存取，也提供高可用高耐用等特性，簡單來說，除非系統人為疏失，否則你資料放在上面不太可會不見(例如電腦硬碟壞掉)。以下就來看看EFS有什麼有據功能吧。
tags: ["aws"]
---

EFS是為Amazon EC2環境提供的檔案系統儲存服務，就像是在雲端上使用NAS服務一樣，能夠進行檔案共享  

- 依據使用者需求自動調整儲存空間，並按照`實際使用容量`與`傳輸量`收費
- 可隨需彈性地擴展且不會中斷應用程式
- 隨著您新增和移除檔案自動擴展和縮減
- 它的設計旨在提供數千個 Amazon EC2 執行個體的大量平行共享存取
- 讓應用程式達到高標準的彙總輸送量和 IOPS
- 地區性服務，其設計目的是提供高可用性和耐久性，以冗餘方式跨多個可用區域存放資料

# EFS 基本實作範例 
動手實作感受一下EFS建立流程 :  
[建立網路檔案系統](https://aws.amazon.com/tw/getting-started/tutorials/create-network-file-system/)


# AWS S3 EBS和EFS的比較差異

| AMAZON S3                  | AMAZON EBS                                | AMAZON EFS                                           |
| -------------------------- | ----------------------------------------- | ---------------------------------------------------- |
| Can be publicly accessible | Accessible only via the given EC2 Machine | Accessible via several EC2 machines and AWS services |
| Web interface              | File System interface                     | Web and file system interface                        |
| Object Storage             | Block Storage                             | Object storage                                       |
| Scalable                   | Hardly scalable                           | Scalable                                             |
| Slower than EBS and EFS    | Faster than S3 and EFS                    | Faster than S3, slower than EBS                      |
| Low Price                  | Normal Price                              | Expensive                                            |
| `Good for storing backups` | `It meant to be EC2 drive`                | `Good for shareable applications and workloads`      |


# 網路上找的一些EFS應用的架構圖
![](../1.jpg)  
![](../2.png)    
![](../3.png)  

看完一些架構圖後應該會有些不一樣的想法   
AWS在儲存解決方案上提供很多工具 :   
S3, EBS, EFS, Glacier, Snowball, Storage Gateway  
一般來說常遇到的應該是前四個  
如何正確選用AWS儲存工具 取決於你的架構  
一個良好的架構可以替企業省下不少維運管理費  
`『好的架構可以從每個月帳單費用看得出來』`

回歸正題，如何應用EFS到你的系統？  
就我的觀察  
多個EC2之間可以大量平行存取 -> 這是最重要的  
當系統架構有使用Auto Scaling, ELB時  
每個EC2裡的應用程式所產生的log不可能都放在自己local EBS storage  
這時就可以透過掛載EFS  
讓所有EC2所產生的log 統一丟到EFS  
EC2 -> EFS的速度比 EC2 -> S3快  
所以存取log很適合應用在這個場景  
透過這個方式  
- EC2單純只留下應用程式功能
- EFS負責Log
- S3負責Assets(images, files)

再進階一點的話  
若您的Log資料量很大  
我們還可以透過Data Pipeline, Lambda工具  
定期將EFS上的log打包送到S3  
進而降低我們的儲存費用  
這取決於你的系統業務邏輯  
將比較常存取的log和不常存取的log做個區分  


# Reference 
- [AWS EBS, S3和EFS的区别](https://www.xiaopeiqing.com/posts/2932.html)
- [Amazon Elastic File System - 用戶指南](https://docs.aws.amazon.com/zh_cn/efs/latest/ug/whatisefs.html)
- [Amazon EFS：運作方式](https://docs.aws.amazon.com/zh_tw/efs/latest/ug/how-it-works.html)
- [Hosting a legacy application on a high availability cluster with AWS](https://enrise.com/2017/01/hosting-legacy-application-on-high-availability-cluster-with-aws/)
