---
title: '如何使用phpdoc'
date: 2018-10-15T18:18:00+08:00
image: "/img/default/phpdoc.png"
description: 現在開始要逐漸要求自己寫代碼的規範，並且follow phpdoc製作好的api文件
tags: ["php", "phpdoc"]
---

## 如何寫php註解
網上資料很多  
這裡就借用前人的智慧 :
 - [點燈坊 - 如何使用PHPDoc寫註解?](https://oomusou.io/phpstorm/phpstorm-phpdoc/)
 - [wiki - PHPDoc](https://zh.wikipedia.org/wiki/PHPDoc)

## Install phpdoc 
官網上提供三種安裝方法 pear, phar, composer  
原本想使用composer  
但要安裝的依賴包太多，我試了好久還是沒辦法(以後再說)  
所以我們選擇最簡單的方法 `phar` : 
1. 直接下載 : [http://www.phpdoc.org/phpDocumentor.phar](http://www.phpdoc.org/phpDocumentor.phar.)
2. github : [phpDocumentor /phpDocumentor2](https://github.com/phpDocumentor/phpDocumentor2/releases) 
> 目前v3版還有問題，建議使用v2


## 產生api document
windows環境下執行 : 
```
cd to/your/project
php C:\phpdoc\phpDocumentor.phar -d ./app -d ./tests -t docs/app 
```
> 註 : -d, -f 可以多次使用

完成後就可以在`docs/app`底下看到`index.html`

![](../1.png)