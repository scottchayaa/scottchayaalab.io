---
title: '如何設定VSCode XDebug在laradock環境上'
date: 2018-10-16T23:39:00+08:00
image: "/img/default/php.png"
description: laradock是為laravel在docker上打造的php環境，本篇介紹如何使用vscode xdebug工具搭配laradock做為開發環境
tags: ["php", "laradock", "xdebug", "vscode"]
---



# Laradock Setting

open `laradock/workspace/xdebug.ini`, `laradock/php-fpm/xdebug.ini`
```ini
xdebug.remote_host=10.0.75.1    # [your local ip address] or 'dockerhost' (see on docker-compose.yml: extra_host)
xdebug.remote_connect_back=0
xdebug.remote_port=9000
xdebug.idekey=VSCODE

xdebug.remote_autostart=1
xdebug.remote_enable=1
xdebug.cli_color=1
xdebug.profiler_enable=1
xdebug.profiler_output_dir="~/xdebug/phpstorm/tmp/profiling"

xdebug.remote_handler=dbgp
xdebug.remote_mode=req

xdebug.var_display_max_children=-1
xdebug.var_display_max_data=-1
xdebug.var_display_max_depth=-1
```

open `.env`
```
WORKSPACE_INSTALL_XDEBUG=true
PHP_FPM_INSTALL_XDEBUG=true

DOCKER_HOST_IP=[your local ip address]
```

rebuild `workspace`, `php-fpm`
```
docker-compose build --no-cache workspace php-fpm
```

## VSCode Setting

 - 安裝[PHP Debug](https://marketplace.visualstudio.com/itemdetails?itemName=felixfbecker.php-debug) Pluging

 - VSCode → 左邊功能項目`偵錯(Debug)` → 點選`齒輪` → 設定或修正`launch.json` 
```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Listen for XDebug",
            "type": "php",
            "request": "launch",
            "pathMappings": {
                "/var/www/laravel_project_name": "${workspaceRoot}"
            },
            "port": 9000
        },
        {
            "name": "Launch currently open script",
            "type": "php",
            "request": "launch",
            "program": "${file}",
            "cwd": "${fileDirname}",
            "port": 9000
        }
    ]
}
```

> `type`, `request`, `port` 這三個參數是固定的  
> `pathMappings`要設定你docker workspace上的laravel專案路徑  
> 因為要與你的remote workspace互相映射  
> 簡單來說remote的laravel專案放在`/var/www/laravel_project_name`  
> 要與你的本機laravel專案 `C:/laravel_project_name` 對應到  
> 然後這邊會使用簡寫變數 `${workspaceRoot}`取代上面的路徑  


## 執行效果
![](../1.gif)



