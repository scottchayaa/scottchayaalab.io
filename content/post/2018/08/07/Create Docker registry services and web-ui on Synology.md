---
title: 'Create Docker registry and web-ui on Synology'
date: 2018-08-07T18:43:00+08:00
image: "/post/2018/08/07/07-1.png"
description: Synology NAS Plus版本提供Docker服務，可以輕鬆在NAS部屬多種VM服務，本篇介紹如何使用Synology Docker架設private registry和registry web服務
tags: ["synology", "docker"]
---

## Download registry image
docker-hub : `registry`
![](../07-2.png)

## Download registry-web image
docker-hub : `hyper/docker-registry-web`
![](../07-3.png)


## registry setting

create `[registry-path]/config.yml`
```yaml
version: 0.1
log:
  fields:
    service: registry
storage:
  delete:
    enabled: true     # open delete api
  cache:
    blobdescriptor: inmemory
  filesystem:
    rootdirectory: /var/lib/registry
http:
  addr: :5000
  headers:
    X-Content-Type-Options: [nosniff]
health:
  storagedriver:
    enabled: true
    interval: 10s
    threshold: 3
auth:
  htpasswd:
    realm: basic-realm
    path: /auth/htpasswd    # use apache basic-auth
```

create `htpasswd` file
```shell
docker run --rm -ti xmartlabs/htpasswd <username> <password> > htpasswd
```

Run `registry` container
```
docker run -d -p 28009:5000 -v [registry-path]/images:/var/lib/registry -v [registry-path]/config/config.yml:/etc/docker/registry/config.yml -v [registry-path]/config/htpasswd:/auth/htpasswd --name registry registry:latest
```


## registry-web setting

create `[registry-web-path]/config.yml`
```yaml
# Default values for yml config
registry:
  # Docker registry url
  url: 'http://[your_ip]:[your_port]/v2'
  # web registry context path
  # empty string for root context, /app to make web registry accessible on http://host/app                                 
  context_path: ''
  # Trust any SSL certificate when connecting to registry
  trust_any_ssl: false
  #  base64 encoded token for basic authentication, ex: base64encode('admin:1234')
  basic_auth: '[your_base64encode]'
  # To allow image delete, should be false
  readonly: false  
  # Docker registry fqdn
  name: '[your_full_dns]'
  # Authentication settings
  auth:
    # Enable authentication
    enabled: false
    # Allow registry anonymous access
    # allow_anonymous: true # not implemented
    # Token issuer
    # should equals to auth.token.issuer of docker registry
    issuer: 'test-issuer'
    # Private key for token signing
    # certificate used on auth.token.rootcertbundle should signed by this key
    key: /config/auth.key
```

Run `registry-web` container
```
docker run -d -p 28008:8080 --link registry:registry -v [registry-web-path]/config/config.yml:/conf/config.yml --name registry-web hyper/docker-registry-web:latest
```

## 備註
1. 存取private registry前，需先執行`docker login -u <username> -p <password> <registry.domain.com>`
2. 本章節的`<registry.domain.com>`使用synology reverse proxy方式完成，架構如圖
![](../07-4.png)  
如果不用reverse proxy方式的話，請參考[registry-deploying-certificate](https://docs.docker.com/registry/deploying/#get-a-certificate)
3. 要注意`yml`的縮排格式
4. `docker --link` 用法請[參考](https://philipzheng.gitbooks.io/docker_practice/content/network/linking.html)
5. **請自行將上述docker run command轉化成synology docker ui操作**

## Conclusion
`hyper/docker-registry-web`太吃CPU和MEM資源，不知道他java是怎麼寫的，而且操作介面有點陽春。之後打算自己寫一套Laravel version的registry-web。

## Reference
 - [https://docs.docker.com/registry/deploying/](https://docs.docker.com/registry/deploying/)
 - [hyper/docker-registry-web](https://hub.docker.com/r/hyper/docker-registry-web/)
 - [registry configuration](https://github.com/docker/distribution/blob/master/docs/configuration.md)
 - [setting registry htpasswd](https://docs.docker.com/registry/configuration/#htpasswd)