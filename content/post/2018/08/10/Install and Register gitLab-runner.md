---
title: 'Install & Register gitlab-runner to your GitLab'
date: 2018-08-10T17:25:00+08:00
image: "/post/2018/08/10/10-1.png"
description: 安裝GitLab runner的心路歷程
tags: ["gitlab", "docker"]
---

## Install gitlab-runner
```shell
docker run -d --name <your_runner_name> --restart always \
  -v /path/to/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```

## Register to your GitLab
```shell
docker exec -it <your_runner_name> gitlab-runner register \
  --non-interactive \
  --url "http://<your_gitlab.com>/ci" \
  --registration-token "zZDBGW3cdoZBFY2ifYkt" \
  --executor "docker" \
  --docker-image alpine:3 \
  --description "<your_runner_name>" \
  --tag-list "<runner_tag1>,<runner_tag2>..." \
  --run-untagged \
  --locked="false"
```

## Enable dind services
vi /path/to/config/config.toml  
or  
docker exec -it pbd-ubuntu-shared-runner vi /etc/gitlab-runner/config.toml

modify `volumes = ["/cache"]` to `volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]`  

```
concurrent = 1
check_interval = 0

[[runners]]
  name = "##"
  url = "http://##.##/ci"
  token = "##"
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "alpine:3"
    privileged = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    shm_size = 0
  [runners.cache]
```

PS : I don't know why `volumes` must be modified, tell me why if someone known it. I found this solution from [here](https://gitlab.com/gitlab-org/gitlab-runner/issues/1986).

After that, you can use services `dind` in `.gitlab-ci.yml`
```
services: 
  - docker:dind
```

## Reference
 - [Install GitLab Runner](https://docs.gitlab.com/runner/install/)
 - [Run GitLab Runner in a container](https://docs.gitlab.com/runner/install/docker.html)
 - [Registering Runners](https://docs.gitlab.com/runner/register/index.html#one-line-registration-command)
 - [GitLab Runner Commands](https://docs.gitlab.com/runner/commands/)
 - [toml - Advanced configuration](https://docs.gitlab.com/runner/configuration/advanced-configuration.html)