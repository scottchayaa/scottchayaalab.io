---
title: 'Update auto scaling group and create AMI by AWS-CLI '
date: 2018-08-23T23:26:00+08:00
image: "/post/2018/08/23/23-1.png"
description: 使用aws-cli建立EC2 snapshot和ami，並更新auto scaling group設定檔
tags: ["aws"]
---

## Create Snapshot from Instance
```sh
aws ec2 create-snapshot --volume-id vol-0182923609c438803 --description "demo : aws-cli create this snapshot" --tag-specifications 'ResourceType=snapshot,Tags=[{Key=Name,Value=PBD_Test_docker_demo}]'
```

## Check Snapshot Info
```shell
aws ec2 describe-snapshots --snapshot-id snap-092b00c7d59aefea9
```

## Create AMI from Shapshot
```sh
aws ec2 register-image \
--name "PBD_Test_demo_20180823_2" \
--description "demo : aws-cli create this ami" \
--architecture x86_64 \
--root-device-name "/dev/sda1" \
--virtualization-type "hvm" \
--block-device-mappings "[
    {
        \"DeviceName\": \"/dev/sda1\",
        \"Ebs\": {
            \"SnapshotId\": \"snap-092b00c7d59aefea9\"
        }
    }
]"
```

## Check AMI Info
```sh
aws ec2 describe-images --image-ids ami-07aa9bdde6da05f01
```

## autoscaling lauch configurations
[describe-launch-configurations](https://docs.aws.amazon.com/cli/latest/reference/autoscaling/describe-launch-configurations.html)
```
aws autoscaling describe-launch-configurations | jq ".LaunchConfigurations[].LaunchConfigurationName"
```

[delete-launch-configuration](https://docs.aws.amazon.com/cli/latest/reference/autoscaling/delete-launch-configuration.html)
```
aws autoscaling delete-launch-configuration --launch-configuration-name <string> 
```

[create-launch-configuration](https://docs.aws.amazon.com/cli/latest/reference/autoscaling/create-launch-configuration.html)
```sh
aws autoscaling create-launch-configuration \
--key-name EC2-IU-APITranslate \
--launch-configuration-name acs-iu-api-$(date +%Y%m%d-%H) \
--image-id ami-2611c459 --instance-type t2.small \
--iam-instance-profile ec2-codedeploy \
--security-groups sg-60392119 \
--block-device-mappings "[
    {
        \"DeviceName\": \"/dev/sda1\",
        \"Ebs\": {
            \"SnapshotId\":\"snap-092b00c7d59aefea9\",
            \"VolumeSize\":20,
            \"VolumeType\":\"gp2\",
            \"DeleteOnTermination\":true
        }
    }
]"

```
**VolumeType**  
`gp2` : General Purpose SSD  
`io1` : Provisioned IOPS SSD  
`st1` : Throughput Optimized HDD  
`sc1` : Cold HDD  

## IAM Reference
Attach existing policies directly : 
- AmazonEC2FullAccess
- AMFullAccess
- AutoScalingFullAccess
- AmazonS3FullAccess


## Reference
 - [aws cli : autoscaling](https://docs.aws.amazon.com/cli/latest/reference/autoscaling/index.html)







