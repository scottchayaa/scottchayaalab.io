---
title: '使用Gitlab CI + AWS CodePipeline方式部屬'
date: 2018-08-01T12:45:00+08:00
image: "/post/2018/08/01/01-1.png"
description: 本章節使用GitLab-CI, AWS CodePipeLine, AWS CodeDeploy, AWS CLI, AWS S3, AWS EC2等工具部屬
tags: ["gitlab", "aws"]
---

## 安裝 AWS CLI
https://docs.aws.amazon.com/zh_cn/cli/latest/userguide/installing.html

## windows 
1. download python
2. 設定 python 環境變數
```
python → C:\Users\Scott\AppData\Local\Programs\Python\Python37
pip → C:\Users\Scott\AppData\Local\Programs\Python\Python37\Scripts
```
3. Install aws-cli
```
pip install --user --upgrade awscli
```
安裝完成時會顯示套件安裝路徑  
將此路徑設定到環境變數
```
C:\Users\Scott>pip install --user --upgrade awscli
Requirement already up-to-date: awscli in c:\users\scott\appdata\roaming\python\python37\site-packages (1.15.66)
Requirement not upgraded as not directly required: botocore==1.10.65 in 
...
```
4. 安裝成功訊息
```
C:\Users\Scott>aws --version
aws-cli/1.15.66 Python/3.7.0 Windows/10 botocore/1.10.65
```
5. Uninstall aws-cli
```
pip uninstall awscli
```

## Ubuntu
1. Install aws-cli
```
# AWS CLI requires python-pip, python is installed by default
sudo apt-get install -y python3  python3-pip
pip3 install awscli
```
2. run aws s3
```
aws configure set aws_access_key_id $ACCESS_KEY
aws configure set aws_secret_access_key $SECRET_KEY
aws configure set default.region ap-southeast-1
aws configure set default.outupt json

uname -a > ~/ec2-info.txt
aws s3 cp ~/ec2-info.txt s3://$BUCKET_NAME/ec2-info.txt
```

## EC2機器需安裝 CodeDeploy agent 和設定 IAM
1. https://docs.aws.amazon.com/codedeploy/latest/userguide/codedeploy-agent-operations-install-ubuntu.html


2. IAM roles參考 : CodeDeploySampleStack
```
{
    "Statement": [
        {
            "Action": [
                "autoscaling:Describe*",
                "cloudformation:Describe*",
                "cloudformation:GetTemplate",
                "s3:Get*"
            ],
            "Resource": "*",
            "Effect": "Allow"
        }
    ]
}
```