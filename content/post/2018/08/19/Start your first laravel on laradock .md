---
title: 'Start your first laravel project on laradock.io'
date: 2018-08-19T00:36:00+08:00
image: "/post/2018/08/19/19-1.png"
description: 站在巨人的肩膀上，使用laradock快速建構你第一個laravel Demo site
tags: ["laravel", "docker", "laradock"]
---


## Pre-required
 - Install [docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-convenience-script)
 - Install [docker-compose](https://docs.docker.com/compose/install/#install-compose)

```
curl -fsSL get.docker.com -o get-docker.sh
sh get-docker.sh
```
```
sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
```
```
sudo usermod -aG docker [your-user]
```


## Install Laradock
```
cd ~
git clone https://github.com/LaraDock/laradock.git
```

```
cd laradock
cp env.example .env
```
`.env`一開始按照預設，之後熟了可以自行修改


## Modify some config

Create ~/Project
```
mkdir -p ~/Project
```

vi ~/laradock/.env
```
APP_CODE_PATH_HOST=~/Project
```

~/laradock/nginx
```
cp ~/laradock/nginx/sites/laravel.conf.example ~/laradock/nginx/sites/laravel.conf
mv default.conf default.conf.old
```

## Run docker 
```
docker-compose up -d nginx mysql redis php-worker
```
接下來會跑一段時間安裝環境
雖然沒有指定`-d workspace`，但預設也會建立起來
![](../19-2.png)

## Create laravel project in workspace 

```
docker-compose exec workspace bash
```

```
composer create-project laravel/laravel --prefer-dist laravel

chmod 777 -R laravel/storage
```

## Send Email Demo Code

vi .env
```
REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=YOUR_MAIL_HOST
MAIL_PORT=YOUR_MAIL_PORT
MAIL_USERNAME=YOUR_MAIL_USERNAME
MAIL_PASSWORD=YOUR_MAIL_PASSWORD
MAIL_ENCRYPTION=ssl
MAIL_NAME="Laravel Demo Mail"
MAIL_FROM=${MAIL_USERNAME}
```

vi routes/api.php
```php
Route::get('/demo/mail', "TestController@mail");
```

vi app/Http/Controller/TestController.php
```php
use Mail;

class TestController extends Controller
{
    public function mail()
    {
        $this->dispatch(new \App\Jobs\SendReminderEmail("Hello World"));

        return "send demo email";
    }
}
```

create job for sending demo email
```
php artisan make:job SendReminderEmail
```

vi Jobs/SendReminderEmail.php
```php

protected $content;

public function __construct(string $content)
{
    $this->content = $content;
}

public function handle(Mailer $mailer)
{
    
    $mailer->send(
        'emails.reminder', ['content' => $this->content], function ($m) {
            $m->from(env('MAIL_FROM'), env('MAIL_NAME'));
            $m->to("TARGET_EMAIL")->subject('Demo mail from Laravel');
        }
    );
}
```

vi resources/views/emails/reminder.blade.php
```php
[Content]<br>
{!! $content !!}
```

## Run Demo

```
curl http://127.0.0.1/api/demo/mail
```
執行後，laravel會在`redis`產生一個`SendReminderEmail`的job工作，接著背景工作`php-worker`就會處理寄信的動作

![](../19-3.png)


## Restart docker-compose

```
docker-compose down

docker-compose up -d nginx mysql redis php-worker
```
如果Jobs, resource/views/emails, Controller, .env... 等相關的email有更新，記得一定要重新啟動`php-worker`套用更新後的程式

## Reference
 - [Laradock/Documentaition](http://laradock.io/documentation/)
 - [以 Docker 建置 PHP/Laravel 開發環境](https://ithelp.ithome.com.tw/articles/10194127)
 - [laradock架設筆記](http://malagege.logdown.com/posts/3523831)