---
title: 'laravel 5.4 Passport show "encryption key error : You must set the encryption key going forward to improve the security of this library"'
date: 2018-07-02T18:17:00+08:00
tags: ["laravel" ,"php"]
---

## laravel passport 安裝設定完後，若使用上出現下面錯誤
![](../02-1.png)

>You must set the encryption key going forward to improve the
security of this library - see this page for more information
https://oauth2.thephpleague.com/v5-security-improvements/

## 解決方法
[http://www.imc4.com/archives/5.html](http://www.imc4.com/archives/5.html)

更新 /vendor/laravel/passport/src/PassportServiceProvider.php 中的makeAuthorizationServer方法: 
```php
public function makeAuthorizationServer()
{
    $server = new AuthorizationServer(
        $this->app->make(ClientRepository::class),
        $this->app->make(AccessTokenRepository::class),
        $this->app->make(ScopeRepository::class),
        'file://'.Passport::keyPath('oauth-private.key'),
        'file://'.Passport::keyPath('oauth-public.key')
    );
    $server->setEncryptionKey(env('APP_KEY'));
    return $server;
}
```

## 待驗證問題
 - 上述出現的錯誤，所使用的版本是 "laravel/passport": "^2.0"
 - 目前官方已更新到，laravel/passport=~4.0，[https://laravel-china.org/docs/laravel/5.5/passport/1309](https://laravel-china.org/docs/laravel/5.5/passport/1309)
 - 需要驗證4.x以上是否有解決上面的問題