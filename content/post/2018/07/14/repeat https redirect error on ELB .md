---
title: 'Repeat https redirect error on ELB'
date: 2018-07-14T22:42:00+08:00
tags: ["laravel", "aws"]
---

## 原本我的後台管理畫面應該是長這樣 : 
![](../14-1.png)

## 結果不知道為什麼跑版 : 
![](../14-2.png)


## 尋找問題

laravel後台系統是放在AWS上，並且有使用ELB, ACM(SSL)的功能

原先的.htaccess設定

```shell
<IfModule mod_rewrite.c>
    ... (略)

    # HTTPS redirect
    RewriteCond %{HTTPS} off
    RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI} [R,L]
</IfModule>
```

後來把HTTPS redirect的部分註解掉後，變成上面圖2跑版的問題

研判問題點應該是出在Laravel與HTTPS相關的問題上


## 解決方法

研究參考其他人的文章後，總結一下問題與解法

1. Client → ELB (443), Amazon SSL
2. ELB → EC2 (80), ELB與EC2溝通時會轉換
3. EC2的.htaccess若有HTTPS redirect, 則會不斷的與步驟1重新導向, 然後出現重新導向次數過多的錯誤
4. 判斷laravel .env APP_ENV, 若為"production"時, 則將Route URL轉換為https

.env
```
APP_ENV=production
```

app\Providers\AppServiceProvider.php
```php
public function boot()
{
    $this->app['request']->server->set('HTTPS', $this->app->environment() == 'production');
}
```


## Reference
 - [http://g23988.blogspot.tw/2017/06/php-laravellaravel-54-ssl-https.html](http://g23988.blogspot.tw/2017/06/php-laravellaravel-54-ssl-https.html)
 - [https://gist.github.com/Maras0830/c3c3b4175196f3401523484aca424c85](https://gist.github.com/Maras0830/c3c3b4175196f3401523484aca424c85)