---
title: 'AWS Application Load Balancer 實現基於主機名稱的路由分發'
date: 2018-07-05T00:48:00+08:00
tags: ["aws"]
---

## 前言
對比Classic ELB和Application Load Blalancer(ALB)  
Classic ELB只能路由分發到相同網域名的Instanc  
而ALB則可以基於傳入不同網域名，然後路由分發到不同的Target Group Instance  

![](../05-1.png)

## 事前準備
1. create 2 single instances or auto-scaling group : A instance, B instance
2. set apache virtualhost domain on A instance : aaa.example.com
3. set apache virtualhost domain on B instance : bbb.example.com

## 設定方法
## step1 : create 2 target group
![](../05-2.png)

## step2 : create ALB → add listener 80, 443(with ACM) → click `View/edit rules`
![](../05-3.png)

## step3 : insert route rules to your ALB 80, 443 port 
![](../05-4.png)

## step4 : set Route53
到Route53  
將aaa.example.com和bbb.example.com網域A指向至你所建立的ALB

## 結語
使用ALB基於主機名/路徑的流量分發特性  
客戶可以僅使用一組ALB就可實現將流量路由給多組後端服務  
不僅簡化系統架構  
也減輕運維負擔以及優化成本  

當你有新的後端服務也想要使用Load Balancer時  
舊的CLB會因為你是新的Domain  
而必須要多開一台CLB => 每月會多USD20左右(貴)  
使用ALB的話  
ALB可以很輕鬆將新後端服務加入到Load Balancer底下  

## 參考
[https://aws.amazon.com/cn/blogs/china/aws-alb-route-distribute/](https://aws.amazon.com/cn/blogs/china/aws-alb-route-distribute/)