---
title: 'Setting Laravel env on VSCode'
date: 2018-07-25T16:30:00+08:00
image: "/post/2018/07/25/25-8.png"
description: VSCode開發Laravel推薦使用工具 (持續更新)
tags: ["laravel", "vscode"]
---


# Plugins
需要設定  
 - [phpcs](https://marketplace.visualstudio.com/items?itemName=ikappas.phpcs)  
 - [phpcbf](https://marketplace.visualstudio.com/items?itemName=persoderlind.vscode-phpcbf)  
 - [PHP Intellisense](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-intellisense)  

無需設定  
 - [PHP Debug](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug)  
 - [laravel-blade](https://marketplace.visualstudio.com/items?itemName=cjhowe7.laravel-blade)  
 - [Laravel Blade Snippets](https://marketplace.visualstudio.com/items?itemName=onecentlin.laravel-blade)  
 - [Laravel goto view](https://marketplace.visualstudio.com/items?itemName=codingyu.laravel-goto-view)  
 - [laravel-goto-controller](https://marketplace.visualstudio.com/items?itemName=stef-k.laravel-goto-controller)  
 - [PHP DocBlocker](https://marketplace.visualstudio.com/items?itemName=neilbrayfield.php-docblocker)  
 - [PHP Extension Pack](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-pack)  
 - [PHP Namespace Resolver](https://marketplace.visualstudio.com/items?itemName=MehediDracula.php-namespace-resolver)  
 - [PHPUnit Snippets](https://marketplace.visualstudio.com/items?itemName=onecentlin.phpunit-snippets)  
 - [VSCode PHPUnit](https://marketplace.visualstudio.com/items?itemName=recca0120.vscode-phpunit)  


## phpcs 
phpcs (PHP Code Sniffer): 檢查程式碼標準工具

在windows composer 環境下安裝`php_codesniffer`  
```
cd C:\composer
composer require squizlabs/php_codesniffer
```

Settings加入 
```json
"phpcs.enable": true,
"phpcs.standard": null,
"phpcs.executablePath": "C:/composer/vendor/bin/phpcs.bat",
```


## phpcbf
php程式碼format工具  
與phpcs搭配，安裝`squizlabs/php_codesniffer`同時也會安裝`phpcbf`喔  

Settings加入  
```json
"phpcbf.enable": true,
"phpcbf.standard": null,
"phpcbf.executablePath": "C:/composer/vendor/bin/phpcbf.bat",
```

> 安裝設定完後，按下右鍵 → Format Document → php程式碼就會自動排版 `(超級好用)`

---

## laravel-blade
This plugin helps in rendering Blade syntax with highlights and emmet support. In my experience, without the support of this plugin, emmet doesn’t perform well in Blade files as it expands HTML tags without indentations or line breaks.

---

## Laravel goto View
This plugin helps us to quickly jump to view file by pressing `Alt+LMB`. A tremendously useful plugin for traversing to view files in complex apps.

---

## Laravel goto Controller
Similar to goto View plugin, this plugin helps us traverse to Controllers by pressing `Alt+LMB` .

---

## Laravel Blade Snippets
This is an extremely useful plugin for Laravel development. You can use emmet-like expansions to Laravel’s Blade tags. For example :

`@extends()` can be expanded from `b:extend` . For enabling tab-to-expand feature, please refer the settings file I’ve attached.

---

## PHP Intellisense
These plugins are used for code completion and linting. Very little explanation is required for these plugins as they are pretty standard.
 - Find all symbols : `ctrl + P` → input `@test` → you can find test symbols in opening file (function, class, property...)

---


## Reference
 - [https://medium.com/@rohan_78316/how-to-setup-visual-studio-code-for-laravel-php-276643c3013c](https://medium.com/@rohan_78316/how-to-setup-visual-studio-code-for-laravel-php-276643c3013c)