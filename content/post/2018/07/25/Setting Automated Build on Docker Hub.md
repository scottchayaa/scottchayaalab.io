---
title: 'Setting Automated Build on Docker Hub'
date: 2018-07-25T15:30:00+08:00
tags: ["docker"]
---

## Automated Build
Automated Build 是 Docker Hub 提供的功能
當你update push Dockerfile to Github repo 時
就馬上根據 Dockerfile 幫你 build 一個新的 image:tag

## step1: 首先到 Docker Hub 右上角點選 Create Automated Build  
![](../25-1.png)

## step2: 設定帳戶與Github連結
![](../25-2.png)
![](../25-3.png)

## step3: 選擇你Github上的docker repo
![](../25-4.png)

## step4: 設定Build Settings
例如：每次 master 有新的 commit 時就自動 build 一個 image:latest
![](../25-5.png)

## step5: Push 新版本到 Github 上
你會在Build Details看到正在更新的queues
![](../25-6.png)