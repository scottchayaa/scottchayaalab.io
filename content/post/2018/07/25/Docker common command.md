---
title: 'Docker 常用指令'
date: 2018-07-25T16:20:00+08:00
tags: ["docker"]
---

## [Docker Login](https://docs.docker.com/engine/reference/commandline/login/)
> docker login [OPTIONS] [SERVER]
```
docker login  
docker login registry.gitlab.com
```

---

## [Docker pull](https://docs.docker.com/engine/reference/commandline/pull/)
> docker pull [OPTIONS] NAME[:TAG|@DIGEST]
```
docker pull gitlab/gitlab-ce:latest
docker pull php:7.2
docker pull mysql:5.6
docker pull registry.gitlab.com/scottchayaa/laravel-ci:latest
```

---

## [Docker build](https://docs.docker.com/engine/reference/commandline/build/)
> docker build [OPTIONS] PATH | URL | -

```
docker build -t example:latest .
```

How to use --build-arg in Dockerfile
```
docker build \
  --build-arg TMP_DIR=tmp_example1 \
  -t example:latest .
```
Dockerfile
```
From php:7.1
ARG TMP_DIR
ADD ./${TMP_DIR:tmp_example} /tmp/
RUN ln -s /tmp/${TMP_DIR}
CMD echo Hello world

# ${TMP_DIR:tmp_example} => default value is "tmp_example"
```

---

## [Docker rm](https://docs.docker.com/engine/reference/commandline/rm/)
> docker rm [OPTIONS] CONTAINER [CONTAINER...]

```
# Remove container which name is gitlab
docker rm gitlab 

# Remove container which status is exited
docker rm $(docker ps -q -f status=exited)

# Remove all stopped containers
docker rm $(docker ps -a -q)

# Remove a container and its volumes
docker rm -v redis
redis
```

---

## [Docker rmi](https://docs.docker.com/engine/reference/commandline/rmi/)
> docker rmi [OPTIONS] IMAGE [IMAGE...]
```
# Remove image id = fd484f19954f
docker rmi -f fd484f19954f

# Remove image tag = <none> (safe)
docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
```

---

## [Docker run](https://docs.docker.com/engine/reference/commandline/run/)
> docker run [OPTIONS] IMAGE[:TAG|@DIGEST] [COMMAND] [ARG...]

Important Options :
Name, shorthand | Description
--- | ---
--publish , -p | Publish a container’s port(s) to the host
--privileged | container內的root擁有真正的root權限
--name | Assign a name to the container
--volume , -v | Bind mount a volume
--detach , -d | Run container in background and print container ID
--cpus | Number of CPUs, ex: 0.3, 1, 2.5...
--restart | Restart policy to apply when a container exits, ex: `no(default)`, `always` : [Restart policies](https://docs.docker.com/engine/reference/commandline/run/#restart-policies---restart)

```
# run php
docker run -dit --name php php:7.1

# run gitlab
docker run --detach \
    --hostname gitlab.example.com \
    --publish 443:443 --publish 80:80 --publish 22:22 \
    --name gitlab \
    --restart always \
    --volume /srv/gitlab/config:/etc/gitlab \
    --volume /srv/gitlab/logs:/var/log/gitlab \
    --volume /srv/gitlab/data:/var/opt/gitlab \
    gitlab/gitlab-ce:latest

# run example on windows
docker run --detach ^
    --hostname gitlab.example.com ^
    --publish 28443:443 --publish 28080:80 --publish 28022:22 ^
    --name gitlab ^
    --restart always ^
    --volume D:/Docker/gitlab-ce_latest/config:/etc/gitlab ^
    --volume D:/Docker/gitlab-ce_latest/logs:/var/log/gitlab ^
    --volume D:/Docker/gitlab-ce_latest/data:/var/opt/gitlab ^
    gitlab/gitlab-ce:latest
```

---

## [Docker exec](https://docs.docker.com/engine/reference/commandline/exec/)
> docker exec [OPTIONS] CONTAINER COMMAND [ARG...]

```
docker exec -it php /bin/bash
```

---

## [Docker top](https://docs.docker.com/engine/reference/commandline/top/)
Display the running processes of a container
> docker top CONTAINER 

```
docker top ubuntu 
```

---

## [Docker stats](https://docs.docker.com/engine/reference/commandline/stats/)
> docker stats [OPTIONS] [CONTAINER...]

```
$ docker stats

CONTAINER ID        NAME                                    CPU %               MEM USAGE / LIMIT     MEM %               NET I/O             BLOCK I/O           PIDS
b95a83497c91        awesome_brattain                        0.28%               5.629MiB / 1.952GiB   0.28%               916B / 0B           147kB / 0B          9
67b2525d8ad1        foobar                                  0.00%               1.727MiB / 1.952GiB   0.09%               2.48kB / 0B         4.11MB / 0B         2
e5c383697914        test-1951.1.kay7x1lh1twk9c0oig50sd5tr   0.00%               196KiB / 1.952GiB     0.01%               71.2kB / 0B         770kB / 0B          1
4bda148efbc0        random.1.vnc8on831idyr42slu578u3cr      0.00%               1.672MiB / 1.952GiB   0.08%  
```

---

## [Docker start](https://docs.docker.com/engine/reference/commandline/start/)

---

## [Docker stop](https://docs.docker.com/engine/reference/commandline/stop/)

---

## [Docker kill](https://docs.docker.com/engine/reference/commandline/kill/)

---

## Reference
- https://docs.docker.com/
- https://blog.gtwang.org/linux/docker-commands-and-container-management-tutorial/



