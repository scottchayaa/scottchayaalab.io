---
title: 'AWS CodePipeline 和 CodeDeploy 使用教學'
date: 2018-07-30T12:00:00+08:00
image: "/post/2018/07/30/30-1.png"
description: 本篇教學參考 Tutorial Create a Simple Pipeline (Amazon S3 Bucket) https://docs.aws.amazon.com/codepipeline/latest/userguide/tutorials-simple-s3.html
tags: ["aws"]
---

## 1. Create S3 Bucket

## 1.1. Bucket Name : `codepipeline-demo`

## 1.2. Choose region : Asia Pacific(Singapore)

## 1.3. Choose `Version > Enable versioning > save`
啟用版本控制後，Amazon S3 會在存儲桶中存儲每個對象的每個版本。

## 1.4 Create bucket

## 2. Download Example Code

## 2.1. [範例下載](https://github.com/aws-samples/aws-codepipeline-s3-codedeploy-linux/blob/master/dist/aws-codepipeline-s3-aws-codedeploy_linux.zip)  
特別注意，zip根目錄必須有`appspec.yml`檔，如果沒有的話CodeDeploy會失敗  
本篇雖然是使用範例，有興趣的人也可以自己改寫新增相關符合專案的需求喔

## 2.2. 將`aws-codepipeline-s3-aws-codedeploy_linux.zip`上傳到S3 `codepipeline-demo`位置裡


## 3. IAM設定
 - IAM > Roles > Create role

## 3.1 為 EC2 創建 IAM role
 - role name : ec2-codedeploy-demo
 - Select type of trusted entity : `EC2`
 - Attach permissions policies : 
  1. 現成 : `AmazonS3ReadOnlyAccess`  
  2. 自建
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "s3:Get*",
                "s3:List*"
            ],
            "Effect": "Allow",
            "Resource": "*"
        }
    ]
}
```

## 3.2 為 CodeDeploy 創建 IAM role
 - role name : codedeploy-demo
 - Select type of trusted entity : `CodeDeploy`
 - Attach permissions policies : 
  1. 現成 : `AWSCodeDeployRole`  


## 4. Create EC2 with IAM Role : `ec2-codedeploy-demo`


## 5. AWS CodeDeploy設定

## 5.1. 打開 [AWS CodeDeploy 控制台](https://console.aws.amazon.com/codedeploy)

## 5.2. 選擇 `Create Application`

參考配置
```
## Create application
--------------------------------------------
Application name* : codedploy-demo
Compute Platform* : EC2/On-premises
Deoployment group name* : demo-group1


## Deployment type
--------------------------------------------
[v] In-place deployment
[ ] Blue/green deployment


## Environment configuration
--------------------------------------------
[Amazon EC2 instances]
Key         Value
Name        http-demo


## Deployment configuration
--------------------------------------------
Deployment configuration : CodeDeployDefault.OneAtATime


## Service role
--------------------------------------------
Service role ARN* : arn:aws:iam::979xxxxxx660:role/codedeploy-http-demo

※ 選擇 IAM > Roles > Trusted entities = codedeploy 的role (步驟3建立)


```


## Reference
 - [https://docs.aws.amazon.com/zh_cn/codedeploy/latest/userguide/welcome.html#welcome-deployment-overview](https://docs.aws.amazon.com/zh_cn/codedeploy/latest/userguide/welcome.html#welcome-deployment-overview)
 - [https://docs.aws.amazon.com/codepipeline/latest/userguide/tutorials-simple-s3.html](https://docs.aws.amazon.com/codepipeline/latest/userguide/tutorials-simple-s3.html)