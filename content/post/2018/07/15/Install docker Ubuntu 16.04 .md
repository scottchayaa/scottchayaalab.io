---
title: 'Install docker on Ubuntu16.04'
date: 2018-07-15T17:20:00+08:00
tags: ["docker"]
---

# Installing Docker CE
AWS EC2  
user : ubuntu

**Install using the repository**
```sh

$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

$ sudo apt-get update

$ sudo apt-get install docker-ce docker-ce-cli containerd.io

# Verify that Docker CE is installed correctly by running the hello-world image.
$ sudo docker run hello-world
```

**Install using the convenience script (EASY)**
```sh
$ curl -fsSL https://get.docker.com -o get-docker.sh
$ sudo sh get-docker.sh
```

如果你希望非root身分也能執行`docker`，那你需要將你的user加入到Docker group底下  

```sh
sudo usermod -aG docker ${USER}
```

> 重新登入ssh就會套用


# Install Docker-compose

```sh
#Run this command to download the current stable release of Docker Compose: 
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# Apply executable permissions to the binary:
sudo chmod +x /usr/local/bin/docker-compose
```

# Reference
 - [Install Docker CE](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce)
 - [Install Compose](https://docs.docker.com/compose/install/)