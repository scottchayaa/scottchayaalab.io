---
title: 'GitLab CI/CD Variables'
date: 2018-09-02T22:09:00+08:00
image: "/post/2018/09/02/02-1.png"
description: GitLa-CI 提供很多系統預設環境變數，這邊幫大家整理一些比較常用的變數以及要注意適用的版本，善用系統提供的變數，可以大幅省去自訂些奇怪變數的問題喔
tags: ["gitlab"]
---

## Demo gitlab repo
下面以 https://gitlab.com/scottchayaa/laravel-ci.git 做為示範

## [Predefined variables (Environment variables)](https://docs.gitlab.com/ee/ci/variables/)

```
Variable             | GitLab | Runner | Example                                                                                | Description
---------------------|--------|--------|----------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------
CI_COMMIT_REF_NAME   | 9.0    | all    | Branch_Scott                                                                           | branch or tag 原先完整名稱
CI_COMMIT_REF_SLUG   | 9.0    | all    | branch-scott                                                                           | 將branch or tag名稱進行字串處理, ex: 大寫轉小寫, '_'變成'-'
CI_COMMIT_TAG        | 9.0    | 0.5    | <null>                                                                                 | 只有tag上傳更新時才會有
CI_JOB_NAME          | 9.0    | 0.5    | build_test                                                                             | The name of the job as defined in .gitlab-ci.yml
CI_JOB_STAGE         | 9.0    | 0.5    | build                                                                                  | The name of the stage as defined in .gitlab-ci.yml
CI_PROJECT_NAME      | 8.10   | 0.5    | laravel-ci                                                                             | your git project name
CI_PROJECT_NAMESPACE | 8.10   | 0.5    | scottchayaa                                                                            | The project namespace (username or groupname) that is currently being built
CI_PROJECT_PATH      | 8.10   | 0.5    | https://gitlab.com/scottchayaa/laravel-ci                                              | The namespace with project name
CI_PROJECT_PATH_SLUG | 8.10   | 0.5    | https://gitlab.com/scottchayaa/laravel-ci                                              | The namespace with project name(轉換大小寫,'_'→'-')
CI_REGISTRY          | 8.10   | 0.5    | registry.gitlab.com                                                                    | If the Container Registry is enabled it returns the address of GitLab's Container Registry
CI_REGISTRY_IMAGE    | 8.10   | 0.5    | registry.gitlab.com/scottchayaa/laravel-ci                                             | If the Container Registry is enabled for the project it returns the address of the registry tied to the specific project
CI_REGISTRY_USER     | 9.0    | all    | gitlab-ci-token                                                                        | 重要 : gitlab default使用`gitlab-ci-token` username跟自己的registery溝通
CI_REGISTRY_PASSWORD | 9.0    | all    | xxxxxxxxxxxxxxxxxxxx                                                                   | 重要 : gitlab default使用`xxxx` password跟自己的registery溝通(內部會自己轉換辨識)
CI_REPOSITORY_URL    | 9.0    | all    | https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@<br>gitlab.com/scottchayaa/laravel-ci.git |
GITLAB_USER_EMAIL    | 9.0    | all    | mmx112945@gmail.com                                                                    | your login email
GITLAB_USER_LOGIN    | 9.0    | all    | scottchayaa                                                                            | The login username of the user who started the job
```

## Recommend self-defined variables
GitLab CI allows you to define per-project or per-group variables
that are set in the pipeline environment. The variables are stored out of
the repository (not in .gitlab-ci.yml) and are securely passed to GitLab Runner
making them available during a pipeline run. It's the recommended method to
use for storing things like passwords, SSH keys and credentials.

Variable        | Description
----------------|------------------------------------------
SSH_USER        | 部署主機的ssh user
SSH_PRIVATE_KEY | 部署主機的ssh private key
ENV             | laravel .env檔案內容(test, prod會不一樣)
AWS_ACCESS_KEY  | AWS IAM access key
AWS_SECRET_KEY  | AWS IAM secret key
AWS_S3_BUCKET   | S3 bucket name
AWS_REGION      | bucket region, cli region...



## Reference
 - [GitLab Variables](https://gitlab.com/help/ci/variables/README#variables)






