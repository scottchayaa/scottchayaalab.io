---
title: 'Nginx location redirect setting for laravel'
date: 2018-09-22T12:12:10+08:00
image: "/img/default/nginx.png"
description: 原系統網站進行改版後，原先網址prefix做了調整，我們可以透過ngnix config的方式重新導向舊網址到新網址
tags: ["nginx", "laravel"]
---


這裡介紹比較常用到的nginx設定方法與情境：

## 重新導向至www
```
server {
     listen 80;
     server_name example.com;
     return 301 http://www.example.com;
}

server {
     listen 80;
     server_name www.example.com;
}
```

## 重新導向至https
```
server {
    listen 80;
    listen [::]:80;

    server_name www.example.com;

    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl;
    listen [::]:443 ssl;

    server_name www.example.com;

    root /var/www/html;
    index index.php index.html index.htm;

    ssl_certificate /etc/nginx/ssl/www.example.com.crt;
    ssl_certificate_key /etc/nginx/ssl/www.example.key;
}
```

> 註：ssl_certificate => `cat [crt_file] [chain_file] >> [nginx_crt_file]`  
[參考](https://support.comodo.com/index.php?/Knowledgebase/Article/View/1091/0/certificate-installation--nginx)

## 重新導向URI和後面參數(Patterns)
這個很實用  
當你舊網址位置更新時，透過此方法將後段URI和Query String參數一併轉址到新的網址之下
```
location ~ ^/en/(.*) {
    return 301 http://en.example.com.tw/$1$is_args$args;
}

location ~ ^/en/site1/(.*) {
    return 301 http://en.i-u.com.tw/site/$1$is_args$args;
}
```
> location後面可以接正規表達式，正規表達式所承接的變數透過$1, $2...方式取用

## nginx一些配置參考
這篇寫得很好，這邊直接引用一部分：[nginx配置location总结及rewrite规则写法](http://seanlook.com/2015/05/17/nginx-location-rewrite/)

```
location  = / {
  # 精确匹配 / ，主机名后面不能带任何字符串
  [ configuration A ]
}

location  / {
  # 因为所有的地址都以 / 开头，所以这条规则将匹配到所有请求
  # 但是正则和最长字符串会优先匹配
  [ configuration B ]
}

location /documents/ {
  # 匹配任何以 /documents/ 开头的地址，匹配符合以后，还要继续往下搜索
  # 只有后面的正则表达式没有匹配到时，这一条才会采用这一条
  [ configuration C ]
}

location ~ /documents/Abc {
  # 匹配任何以 /documents/Abc 开头的地址，匹配符合以后，还要继续往下搜索
  # 只有后面的正则表达式没有匹配到时，这一条才会采用这一条
  [ configuration CC ]
}

location ^~ /images/ {
  # 匹配任何以 /images/ 开头的地址，匹配符合以后，停止往下搜索正则，采用这一条。
  [ configuration D ]
}

location ~* \.(gif|jpg|jpeg)$ {
  # 匹配所有以 gif,jpg或jpeg 结尾的请求
  # 然而，所有请求 /images/ 下的图片会被 config D 处理，因为 ^~ 到达不了这一条正则
  [ configuration E ]
}

location /images/ {
  # 字符匹配到 /images/，继续往下，会发现 ^~ 存在
  [ configuration F ]
}

location /images/abc {
  # 最长字符匹配到 /images/abc，继续往下，会发现 ^~ 存在
  # F与G的放置顺序是没有关系的
  [ configuration G ]
}

location ~ /images/abc/ {
  # 只有去掉 config D 才有效：先最长匹配 config G 开头的地址，继续往下搜索，匹配到这一条正则，采用
    [ configuration H ]
}

location ~* /js/.*/\.js
```

## 語法規則 
`location [=|~|~*|^~] /uri/ { … }`
- `=` 开头表示精确匹配  
- `^~` 开头表示uri以某个常规字符串开头，理解为匹配 url路径即可。nginx不对url做编码，因此请求为/static/20%/aa，可以被规则^~ /static/ /aa匹配到（注意是空格）。  
- `~` 开头表示区分大小写的正则匹配  
- `~*`  开头表示不区分大小写的正则匹配  
- `!~`和`!~*`分别为区分大小写不匹配及不区分大小写不匹配 的正则  
- `/` 通用匹配，任何请求都会匹配到。  



## Reference 
 - [How to do URL Redirects with Nginx](https://www.serverlab.ca/tutorials/linux/web-servers-linux/how-to-redirect-urls-with-nginx/)
 - [nginx配置location总结及rewrite规则写法](http://seanlook.com/2015/05/17/nginx-location-rewrite/)