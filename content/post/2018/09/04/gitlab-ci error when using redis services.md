---
title: 'Gitlab-CI error when using the redis services'
date: 2018-09-04T14:45:00+08:00
image: "/img/default/gitlab.png"
description: gitlab-ci日常工作遇到的問題，本次介紹使用redis services時所遇到的錯誤與解決方法
tags: ["gitlab"]
---

## 原本.gitlab-ci.yml
```yml
test_backend:
  stage: test
  image: laradock/workspace:2.2-${PHP_VERSION}
  services:
    - mysql:${MYSQL_VERSION}
    - redis
  variables:
    MYSQL_ROOT_PASSWORD: "secret"
    MYSQL_DATABASE: "homestead"
```

## 執行後出現錯誤
```
...

There were 2 errors:

1) ExampleTest::testIndex
Predis\Connection\ConnectionException: Connection refused [tcp://redis:tcp://172.17.0.4:6379]

/builds/IU/IUPreorder_front/vendor/predis/predis/src/Connection/AbstractConnection.php:155
/builds/IU/IUPreorder_front/vendor/predis/predis/src/Connection/StreamConnection.php:128
/builds/IU/IUPreorder_front/vendor/predis/predis/src/Connection/StreamConnection.php:178
/builds/IU/IUPreorder_front/vendor/predis/predis/src/Connection/StreamConnection.php:100
...

2) ExampleTest::testCooperation
Predis\Connection\ConnectionException: Connection refused [tcp://redis:tcp://172.17.0.4:6379]

...

```

可以發現錯誤主要原因是`tcp://redis:tcp://172.17.0.4:6379`
可是我的yml乍看之下沒什麼問題啊

## 解決方法
後來找到文章有人說 : **因為docker的系統環境把變量REDIS_PORT設置成了tcp://172.17.0.4:6379**  
所以我們須將`REDIS_PORT`這個變數重新再定義一次

```yml
variables:
  REDIS_PORT: "6379"
```

更新並重新執行CI後就運作正常了


## Reference
 - [Docker中使用Laravel-Predis出错](https://iwww.me/636.html)
 - [GitLab - Using Redis](https://docs.gitlab.com/ee/ci/services/redis.html)