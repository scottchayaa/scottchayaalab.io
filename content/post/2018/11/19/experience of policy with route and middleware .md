---
title: 'Laravel Policies在Route和middleware的綜合使用方式'
date: 2018-11-19T18:43:00+08:00
image: "/img/default/laravel.png"
description: Laravel日常實作小技巧
tags: ["laravel"]
---

# Routers
```php
<?php

Route::put('/list/friend/{mid}', 'ListController@friend_update');
Route::put('/list/friend/{mid}', 'ListController@friend_update1');
Route::put('/list/friend/{member_friend}', 'ListController@friend_update2');
Route::put('/list/friend/{member_friend}', 'ListController@friend_update3')->middleware('can:update,member_friend');
```

# Policy

```php
<?php

namespace App\Policies;

use App\Models\Member;
use App\Models\Member_Friend;
use Illuminate\Auth\Access\HandlesAuthorization;

class Member_FriendPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {}

    public function update(Member $member, Member_Friend $member_friend) 
    {
        return $member->account_id === $member_friend->member_account_id;
    }
}
```


# Controller
```php
<?php


/**
* 1. 一開始的程式碼
*/
public function friend_update(Request $request, int $mid)
{
    $result = $request->user()
        ->getFriends()
        ->where('member_account_id_detail', $mid)
        ->firstOrFail()
        ->update(['customname' => $request->customname]);

    return Common::jsonResponse(
        [ 'success' => true ],
        200
    );
}

/**
* 2. 嘗試套入簡單的policy方法
*/
public function friend_update1(Request $request, int $mid)
{
    $member_friend = Member_Friend::findOrFail($mid);
    if ($request->user()->cant('update', $member_friend)) {
        abort(403);
    }
    $member_friend->update(['customname' => $request->customname]);

    return Common::jsonResponse(
        [ 'success' => true ],
        200
    );
}

/**
* 3. 透過Route使用Member_Friend類別依賴注入
*/
public function friend_update2(Request $request, Member_Friend $member_friend)
{
    $this->authorize('update', $member_friend);
    $member_friend->update(['customname' => $request->customname]);

    return Common::jsonResponse(
        [ 'success' => true ],
        200
    );
}

/**
* 4. 在Route上middleware policy (can:policy_function,model)後
*/
public function friend_update3(Request $request, Member_Friend $member_friend)
{
    $member_friend->update(['customname' => $request->customname]);

    return Common::jsonResponse(
        [ 'success' => true ],
        200
    );
}

```