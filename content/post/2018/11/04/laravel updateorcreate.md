---
title: 'Laravel Model funtion about updateOrCreate'
date: 2018-11-04T21:00:00+08:00
image: "/img/default/laravel.png"
description: Laravel日常實作小技巧
tags: ["laravel"]
---

# 使用情境
今天如果你要`更新或新增`一筆`文章資料`  
你會怎麼做？

# 一般作法
```php
public function updateOrCreate_Artical($id, $data){
    //
    $artical = \App\Models\Artical::find($id);
    if(!artical){
        $data["id"] = $id;
        \App\Models\Artical::create($data);
    } esle {
        $artical->update($data);
    }
}
```
> TIPS :  
這邊使用find找單一資料
同樣的如果是用`where()`, `get()`找多筆資料後  
也是可以使用`create()`, `update()`的方法  
記得要將您要填入的欄位寫入`fillable`  
作為可以被批量賦值的屬性「白名單」


# 有更簡單的方法
```php
public function updateOrCreate_Artical($id, $data){
    $artical = \App\Models\Artical::updateOrCreate(["id" => $id], $data);
}
```


# Conclusion
Laravel常有一些可以省去很多程式碼的小方法
不知道的人就會傻傻地寫一堆邏輯判斷
其實人家都幫你做好了
就差你有沒有多讀讀他家的API文件或Guide Line

