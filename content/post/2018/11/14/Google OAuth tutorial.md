---
title: 'Google+ OAuth 第三方登入'
date: 2018-11-14T15:10:00+08:00
image: "/post/2018/11/14/1.png"
description: 重點摘要Google第三方登入的申請設定步驟和注意事項
tags: ["gcp", "oauth", "laravel"]
---

# 實作完成畫面
![](/post/2018/11/14/2.png)

這個畫面是表示  
「Scottchayaa Labs」這個Google應用程式想要存取你Google帳戶的資訊  
需要你登入確認後做授權給這個應用程式的動作  
若你已經登入的狀況下  
則會跳過輸入帳密的動作  
現在很多網站都有支援「第三方登入」  
主要是方便使用者可以透過自己既有的帳戶資訊  
來註冊登入到此系統(你做的系統)  

# [Google Cloud Platform](https://cloud.google.com/)

- 前往主控台 > **新增專案** > 選擇你的專案  
![](/post/2018/11/14/3.png)


- 導覽選單(左上) > API和服務  
![](/post/2018/11/14/4.png)


# 啟用Google+ API服務

- 資訊主頁 > 啟用API和服務 > 搜尋「google+」 > 找到Google+ API > 啟用
![](/post/2018/11/14/5.png)

> 也可以透過「資料庫」進行搜尋

- 若你沒有在這個專案底下建立「憑證」，接著會獲得一個警告
![](/post/2018/11/14/6.png)

- 啟用**Google+ API**服務後，可以在資訊主頁下方API服務清單看到
![](/post/2018/11/14/7.png)


# 建立憑證

- 在建立OAuth憑證之前 > 需先完成「OAuth同意畫面」資料設定
![](/post/2018/11/14/9.png)

> 這裡有一點要特別注意，OAuth設定相關操作  
> 會需要有自己可以使用的網域  
> 像我自己有 scottchayaa.com.tw  
> sub domain可以訂成像是 :  
> api.scottchayaa.com.tw  
> service.scottchayaa.com.tw  
> blog.scottchayaa.com.tw

> [ 需要填寫的有 ]  
應用程式名稱 : Google授權登入畫面時，顯示的名稱  
應用程式標誌 : Google授權登入畫面時，顯示的Logo  
已授權網域 : OAuth用戶端有用到的domain，這裡需要涵蓋到  
應用程式首頁連結 : 填寫你應用程式的首頁連結，假設你有做一個blog，就放你blog的link  
應用程式隱私權政策連結 : 填寫你寫的隱私權頁面連結，隱私權文案可以上網抓  
應用程式服務條款連結 (選填)

- API金鑰 : 存取Google Cloud Service API服務  
- OAuth用戶端ID : **使用這個**  
![](/post/2018/11/14/8.png)


- 建立OAuth用戶端ID
![](/post/2018/11/14/10.png)

> 1. 應用程式類型 : 網路應用程式(透過網頁OAuth認證)  
1. 名稱 : 這個OAuth程式的識別名稱，不是應用程式的顯示名稱喔  
1. 已授權的JavaScript來源 : 新增你接下來要實作callback api的網域  
1. 已授權的重新導向URI : 新增你實作的callback url  


- 建立完成後會得到OAuth ID and Key
![](/post/2018/11/14/11.png)

> 接下來你會需要ID和Key來用在你的web應用程式  
> 如 : php, python, c#...  
> 後面我們使用php laravel 來做Google OAuth Login實作


# Laravel Socialite Login 

- Install Package

```bash
composer require laravel/socialite
```

- Configuration

```php
<?php

'google' => [
    'client_id' => env('GOOGLE_CLIENT_ID'),         // Your Google Client ID
    'client_secret' => env('GOOGLE_CLIENT_SECRET'), // Your Google Client Secret
    'redirect' => 'http://api.scottchayaa.com/google/auth',
],
```

- Routing

```php
<?php

Route::get('/google/auth', 'SocialiteController@redirectToProvider');
Route::get('/google/auth/callback', 'SocialiteController@handleProviderCallback');
```

- Controller 

```php
<?php

namespace App\Http\Controllers;
use Socialite;

class SocialiteController extends Controller
{
    /**
     * Redirect the user to the Google authentication page.
     *
     * @return 301 
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('google')->user();
        dd($user);
    }
}
```

- 完成後將api.scottchayaa.com發布到線上可以存取的環境
- 瀏覽器上輸入http://api.scottchayaa.com/google/auth 後，就會跳轉到Google授權頁面
- 完成登入授權後，返回到/google/auth/callback ，我們使用laravel dd()顯示使用者授權回來後的資訊
![](/post/2018/11/14/12.png)

> 以下介紹幾個重要欄位資料  
> - token : 可以使用此access_token來獲取用戶端的基本資訊  
> - refreshToken : google oauth好像沒有，不過Facebook, line的oauth會出現此欄位資料，主要用來刷新獲取token  
> - expiresIn : tokene過期時間(秒)  
> - id : 用戶端在google的user id => 這個是唯一值，可以作為我們系統實作第三方登入時的依據，**非重要**  
> - email, nickname, name : 第三方登入後的基礎資料(nickname不一定會有)  
> - avatar : 大頭貼  

# Conclusion
現在其實可以發現  
許多網站和線上遊戲  
都有支援第三方登入  
方便使用者不用記帳號(email)和密碼  
GCP OAuth算是普遍大家都有的服務  
所以開發使用者登入功能時  
別忘了第三方登入這一塊  

