---
title: "AWS EC2 不Stop情況下擴充Volume"
date: 2018-06-28T23:37:00+08:00
tags: ["aws", "linux"]
---

## 1. Modify Volume
去Volumes功能，找到EC2的Volume調整成你想要的大小，並等待跑到100%
本次範例將原先Volume 16G擴充到32G

## 2. SSH登入到EC2
```shell
#查看硬碟分割狀態
lsblk 
```
![](../28-4.png)

會發現xvda有多出空間，但目前還是16G


## 3. 磁區分割延伸

```shell
sudo apt install cloud-utils
sudo growpart /dev/xvda 1
sudo resize2fs /dev/xvda1
```

## 4. 確認硬碟擴充成功

```shell
df -h
```

## Reference
[https://medium.com/@linpoan/%E7%AD%86%E8%A8%98-ebs-%E6%93%B4%E5%AE%B9-253a4b31fb2e](https://medium.com/@linpoan/%E7%AD%86%E8%A8%98-ebs-%E6%93%B4%E5%AE%B9-253a4b31fb2e){:target="_blank"}


