---
title: "如何在Rote53設定Outlook Exchange MX紀錄"
date: 2018-06-28T23:48:00+08:00
tags: ["aws", "office"]
---

## 確認你要設定的MX資訊
登入 office.com → 系統管理 → 安裝程式 → 找到「網域」功能選項 → 選擇你的網域

![](../28-1.png)

這三個都要設定
![](../28-2.png)


## Route53設定MX
設定參考如下
![](../28-3.png)


