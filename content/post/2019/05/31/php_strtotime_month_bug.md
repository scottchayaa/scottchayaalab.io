---
title: 'PHP strtotime +1 month 會多增加 1個月的問題'
date: 2019-05-31T16:30:00+08:00
image: "/img/default/php.png"
description: 之前有寫一個月份遞增功能，今天突然發生的一個bug，原本應該 `Now month +1, +2...`，結果竟然變成`Now month +2, +4...`
tags: ["php"]
---

後來研究發現是`每個月的31號`才會發生的BUG  
REF : [PHP strtotime +1 month adding an extra month [duplicate]](https://stackoverflow.com/questions/14584115/php-strtotime-1-month-adding-an-extra-month#14584265)

# 還原問題 (精簡)

test.php
```php 
<?php
echo date('Y-m-d H:i:s') . "\n";
echo date('Y-m-d H:i:s', strtotime("1 month")) . "\n";
echo date('Y-m-d', strtotime("2 month")) . "\n";
echo date('Y-m-d', strtotime("3 month")) . "\n";
echo date('Y-m-d', strtotime("4 month")) . "\n";
echo date('Y-m-d', strtotime("5 month")) . "\n";
```

執行後
```
root@98006f117e53:~# php ./test.php
2019-05-31 08:56:23  ---> 今天日期
2019-07-01 08:56:23  ---> 原本只要 +1 month, 結果變成 +2 month
2019-07-31
2019-08-31
2019-10-01
2019-10-31
```

> 因為6月只有30天，結果05-30在程式處理時
> `1 month`是`加30天`，所以得到`07-01`
> 顯然不是我們要的結果

# 如何處理

透過 `last day of`這個關鍵字來處理  
雖然會得到每個月最後一日ex: 30、31  
但沒關係，我們主要是要得到正確的`年/月`  

test.php
```php
<?php
echo date('Y-m-d H:i:s') . "\n";
echo date('Y-m-d H:i:s', strtotime("last day of 1 month")) . "\n";
echo date('Y-m-d', strtotime("last day of 2 month")) . "\n";
echo date('Y-m-d', strtotime("last day of 3 month")) . "\n";
echo date('Y-m-d', strtotime("last day of 4 month")) . "\n";
echo date('Y-m-d', strtotime("last day of 5 month")) . "\n";
```

修正後，月分顯示就正常了
```
root@98006f117e53:~# php ./test.php
2019-05-31 09:01:34
2019-06-30 09:01:34
2019-07-31
2019-08-31
2019-09-30
2019-10-31
```
