---
title: 'Javascript Date 的健忘筆記'
date: 2019-05-27T17:30:00+08:00
image: "/img/default/javascript.png"
description: 不管什麼今天寫什麼程式，看到Date就好像得了失憶症一樣，所以整理此篇幫助記憶
tags: ["javascript"]
---

# How to format date (yyyy-mm-dd)

Create `DateFormat.js`
```js
// 對Date的擴充套件，將 Date 轉化為指定格式的String
// 月(M)、日(d)、小時(h)、分(m)、秒(s)、季度(q) 可以用 1-2 個佔位符，
// 年(y)可以用 1-4 個佔位符，毫秒(S)只能用 1 個佔位符(是 1-3 位的數字)
// 例子：
// (new Date()).format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).format("yyyy-M-d h:m:s.S")   ==> 2006-7-2 8:9:4.18
Date.prototype.format = function (fmt) {
  var o = {
    "M+": this.getMonth() + 1, //月份
    "d+": this.getDate(), //日
    "h+": this.getHours(), //小時
    "m+": this.getMinutes(), //分
    "s+": this.getSeconds(), //秒
    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
    "S": this.getMilliseconds() //毫秒
  };
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o)
  if (new RegExp("(" +  k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" +  o[k]).substr(("" + o[k]).length)));
  return fmt;
}

Date.prototype.addSeconds = function(seconds) {
  this.setSeconds(this.getSeconds() + seconds);
  return this;
}

Date.prototype.addMinutes = function(minutes) {
  this.setMinutes(this.getMinutes() + minutes);
  return this;
}

Date.prototype.addHours = function(hours) {
  this.setHours(this.getHours() + hours);
  return this;
}

Date.prototype.addDays = function(days) {
  this.setDate(this.getDate() + days);
  return this;
}

Date.prototype.addMonths = function(months) {
  this.setMonth(this.getMonth() + months);
  return this;
}

Date.prototype.addYears = function(years) {
  this.setFullYear(this.getFullYear() + years);
  return this;
}

function diffSeconds(milliseconds) {
  return Math.floor(milliseconds / 1000);
}

function diffMinutes(milliseconds) {
  return Math.floor(milliseconds / 1000 / 60);
}

function diffHours(milliseconds) {
  return Math.floor(milliseconds / 1000 / 60 / 60);
}

function diffDays(milliseconds) {
  return Math.floor(milliseconds / 1000 / 60 / 60 / 24);
}
```

在html裡面引用`DateFormat.js`

```html
<script src="/js/DateFormat.js"></script>
<script>
  console.log((new Date()).Format("yyyy-MM-dd hh:mm:ss"));
</script>
```

# 日期運算範例

```html
<script src="DateFormat.js"></script>
<script>
  // Now Time
  let now_d = new Date();
  // Specific Time
  var spe_d1 = new Date('August 19, 2018 23:15:30');
  var spe_d2 = new Date('2019-03-12 21:34:00');
  var spe_d3 = new Date('2019/03/12 21:34:00');
  var spe_d4 = new Date(1558947486000); // timestamp millisecond

  console.log([
    now_d.format("yyyy-MM-dd hh:mm:ss"),
    spe_d1.format("yyyy-MM-dd hh:mm:ss"),
    spe_d2.format("yyyy-MM-dd hh:mm:ss"),
    spe_d3.format("yyyy-MM-dd hh:mm:ss"),
    spe_d4.format("yyyy-MM-dd hh:mm:ss"),
  ]);

  // Add days
  console.log([
    now_d.addDays(1).format("yyyy-MM-dd hh:mm:ss"),
    now_d.addDays(-1).format("yyyy-MM-dd hh:mm:ss"),
  ]);

  // Add years
  console.log([
    now_d.addYears(3).format("yyyy-MM-dd hh:mm:ss"),
    now_d.addYears(-3).format("yyyy-MM-dd hh:mm:ss"),
  ]);

  // Date diff function
  console.log(diffSeconds(now_d - spe_d2));
  console.log(diffMinutes(now_d - spe_d2));
  console.log(diffHours(now_d - spe_d2));
  console.log(diffDays(now_d - spe_d2));
</script>
```

# 範例結果
![](/post/2019/05/27/01.png)

# Summary
事先把 `DateFormat.js` 建立好引用後  
Date運算+格式處理就變得比較容易直覺許多  
之前會寫把整坨運算寫成一行 :  
```js
// Add Days
console.log(new Date(now_d.setDate(now_d.getDate() + 3)).Format("yyyy-MM-dd hh:mm:ss"));
```
缺點是很不容易閱讀  
改成使用`addDays()`後就變舒服多了  

最後有關時間相差的運算處理  
由於2個date相減會得到`milliseconds`數值  
所以這邊寫 diff 方法來個別處理  
原本是想寫個date物件來封裝方法  
但如果要寫的話感覺整套`Date()` Class, function, properties 機制就要整個一起納入考量...  
想想決定還是先寫成`diff全域方法`  
主要是記得後面再寫JS時要避開這幾個定義好的diff function

# 後記
後來發現有`moment.js`這個好用的時間處理函式庫  
詳細使用方法引用其他人的文章 :  

 - [moment.js - 關於時間的一切](https://ithelp.ithome.com.tw/articles/10208995)

