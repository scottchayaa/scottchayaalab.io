---
title: 'AWS Workshop : Running Containers on AWS Fargate'
date: 2019-03-14T13:00:00+08:00
image: "/img/default/aws-workshop-series.png"
description: 紀錄aws workshop當天的學習筆記，本次有三個主題，分別為「Fargate實作」、「ECS Trobuleshooting」和「DDD與微服務議題」。
tags: ["aws", "workshop", "ecs", "fargate"]
---

# Introduction
[https://aws.amazon.com/tw/events/workshop-series-tw-06/](https://aws.amazon.com/tw/events/workshop-series-tw-06/)


# Section 1 : Running Containers on AWS Fargate (Hands-on)
[http://fargate-cicd-workshop.s3-website-ap-southeast-1.amazonaws.com/setup.html](http://fargate-cicd-workshop.s3-website-ap-southeast-1.amazonaws.com/setup.html)

## Q1. 什麼時候要用fragate ? 什麼時候用 ec2 launch type?
 - 看公司的系統規範

### Q2. ECS Agent issue
 - Fargate 裡面也有ECS Agent，只是Fargate底層由AWS代管
 - EC2 Launch type need to install agent

### Q3. ECS Task issue
 - 1個 task 最多有10個containers 
 - placement strategies are `best effort`
 - provide cron-like to schedulely run your task


# Section 2 : Troubleshooting
這個環節由AWS Support team的來講解他們實際處理的ECS案件  
透過troubleshooting的方式讓我們了解遇到ECS相關的錯誤時  
該如何分析問題  
透過一步步引導找logs的方式發現問題所在  
簡單來說這個環節就是 : `DEBUG` (超有趣)  

### ECS Troubleshooting CASE1
EC2 無法註冊到 ECS  
→ SSH to EC2，檢查ecs agent 產生的log  
→ log在 /var/logs/ecs  
→ 發現最新的 log 顯示 AccessDenie  
→ EC2加入ECS Role相關權限(Write, Create...)後解決問題  

### CAS2
ALB (忘了, 等PPT補齊)  

### CASE3
Fraget restart again and again  
-> see task error logs  
-> cannot pull image from dokcer hub  
-> assign public ip ?  

### CASE4 : service limit 
Farget limit issue  
-> service task最大上限總和是 50個  


### CASE5
- service -> event
- mem 不夠
- hard mem 配置

### CASE 6
- eni 限制
- fragate => aws support => 50
- ec2 launch type => medium => netwokr 3
- Fargate 只支援 cloud watch log 


# Section 3 : Microservice with Domain Driven Design

微服務  

case: Gilt  

Sage: Alternative for 2 Phase - commit  

`分散式交易議題?`  

`AWS Step Function` => 狀態機服務  

api 狀態補償代碼  

domain driven design => DDD  

DDD Taiwan@FB  


# 後記
實作課上完後
需要把服務關閉+刪除
避免aws cloud產生過多費用

- ECS(Fargate) : 
  - [Pricing](https://aws.amazon.com/fargate/pricing/)
  - 確認Cluster有沒有清除 (內含Service, Tasks)
- Cloud 9 : 
  - 關EC2的概念, 不過他有自動休眠(Stopped)機制, 30分鐘不使用時會自動關, 所以這個其實可以不用刪
- Application Load Balancing : 
  - [Pricing](https://aws.amazon.com/tw/elasticloadbalancing/pricing/)
  - 這個會產生比較多費用, 一定要刪
- Route53 : (重要)
  - 後來發現當天在實作ECS時，也有產生`local.`的private dns
  - 當我嘗試去刪除他時，出現error : `The resource hostedzone/********** can only be managed through servicediscovery.amazonaws.com`
  - 後來有查到解法 : [amazon-archives/service-discovery-ecs-dns : 44#issuecomment-389635471](https://github.com/amazon-archives/service-discovery-ecs-dns/issues/44#issuecomment-389635471)




