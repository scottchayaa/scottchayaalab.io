---
title: '製作一個標準的NPM模組'
date: 2019-07-04T23:30:00+08:00
image: "/img/default/npm.png"
description: 在做專案的當下，有時會想將某些常用的功能做成私有模組，甚至獨立公開出來變成公用模組，達到開源貢獻的精神，此舉利人利己豈不妙哉？ 本篇介紹製作開源模組npm所需要注意的事項以及實作範例。
tags: ["npm", "nodejs"]
---


# NPM account
首先，當然是先註冊一個[NPM](https://www.npmjs.com/)帳號


# 創建一個NPM project

```sh
> mkdir [your_project_name]
> cd [your_project_name]
> npm init

# 可根據步驟填入你想要的參數
package name: (npm_demo)
version: (1.0.0) 0.0.1
description: This is my demo npm module
entry point: (index.js)
test command:
git repository: [your git repository url of npm]
keywords: demo
author: scottchayaa
license: (ISC) MIT

# 完成後就會產生 package.json
```

Create entry point (index.js)  
接下來，我們為這個套件寫2個方法  
```js
module.exports.add = (x, y) => {
  return x + y;
}

module.exports.sum = (items) => {
  let result = 0;
  for (let i of items) {
    result += i;
  }
  return result;
}
```

> 最基本只要設定好 `package.json`和`index.js` 就可以發佈套件了

# 發佈模組
記得要先確認你本機npm是否login
```sh
npm whoami # 先確認是否login
npm login # 若無login，則執行此行
```

準備完後，接下來發佈套件很簡單，只要一行
```
npm publish
```

> package.json 發佈前的注意事項  
> 1. `name` : 不可與其他公開模組的名稱重複
> 2. `version` : 每次更新發佈時，記得版本號要`手動+1`，ex : 0.0.12 → 0.0.13
> 3. `main` : require 模組起始載入的 js file name


# 引用剛發佈的模組

上述完成發佈後  
馬上就來試試你寫的公開套件吧  

```sh
# install your npm
yarn add first-npm-publish-demo 
```

```js
const demo = require('first-npm-publish-demo');
console.log(demo.add(2, 3)); // 5
```

# 補充 : 使用Node Tap單元測試

install mocha, chai
```sh
yarn add -D tap
```

vi package.json
```json
"scripts": {
  "test": "tap test/*.js"
},
```

vi test/index.js
```js
const tap = require('tap');
const app = require("../index");


tap.test('test add()', function (t) {
    t.plan(1);
    t.equal(app.add(1, 2), 3);
});

tap.test('test sum()', function (t) {
    t.plan(1);
    t.equal(app.sum([1, 2, 3]), 6);
});

```

執行測試
```sh
> npm test
```

![](/post/2019/07/04/01.png)


# Summary
在做專案時  
你會發現有些功能你可以完全獨立出來變成模組  
好讓你日後在其他專案上可以再次被輕易使用  
而且這個模組日後有需要更新、修正bug或擴充功能時  
新舊專案都可以一併透過更新模組得到解決  
這就牽扯到是否有良好的設計模式與架構  


不過簡單的觀念的話  
可以試著在你專案內做以下的分類  

 - 工具類模組 : 陣列處理方法, 客製運算方法, 字串處理方法, 排序方法, http工具...等
 - 第三方套件模組 : Facebook, LINE, Twitter 等常用API製作的工具
 - 特殊領域模組 : 影像處理, CLI命令, AI處理 ...等

當然網路上查還是有其他的分類方法  
只要不把功能全部擠在同一個模組，搞得功能四不像就好了  

有時候看到前人寫的Code  
你會發現那種想到什麼就加什麼進去的  
都非常`髒`且非常難`維護`  

良好的架構與設計習慣  
才能讓專案穩定活下去  


# Reference
 - [如何做出一個好的 NodeJS 模組？](https://blog.techbridge.cc/2018/02/14/how-to-make-a-good-node-module/)
 - [單元測試：Mocha、Chai 和 Sinon](https://cythilya.github.io/2017/09/17/unit-test-with-mocha-chai-and-sinon/)
 - [Day5 - 關於 module.exports 的兩三事](https://ithelp.ithome.com.tw/articles/10185083)