---
title: '開始第一個Serverless服務'
date: 2019-02-07T18:00:00+08:00
image: "/post/2019/02/07/cover.png"
description: 無伺服器運算服務(Serverless)，可設定觸發條件後執行特定的程式碼作為事件的回應，並自動透過該服務管理基礎設備(Infrastructure)的運算資源，不必考慮伺服器的類型或選項，只需思考需要使用的多少運算時間來作為費用成本。
tags: ["aws", "serverless", "lambda", "nodejs"]
---

# Serverless Framework
[serverless.com](https://serverless.com/)


# Install

安裝`serverless`
```
npm install serverless -g
```

確認是否安裝完成
```
serverless -v
```

# Create serverless project

透過 `serverless create --help` 查看指令功能、支援的樣板
```sh
> serverless create --help
Plugin: Create
create ........................ Create new Serverless service
    --template / -t .................... Template for the service. Available templates: "aws-clojure-gradle", "aws-clojurescript-gradle", "aws-nodejs", "aws-nodejs-typescript", "aws-alexa-typescript",
"aws-nodejs-ecma-script", "aws-python", "aws-python3", "aws-groovy-gradle", "aws-java-maven", "aws-java-gradle", "aws-kotlin-jvm-maven", "aws-kotlin-jvm-gradle", "aws-kotlin-nodejs-gradle", "aws-scala-sbt", "aws-csharp", "aws-fsharp", "aws-go", "aws-go-dep", "aws-go-mod", "aws-ruby", "azure-nodejs", "cloudflare-workers", "cloudflare-workers-enterprise", "fn-nodejs", "fn-go", "google-nodejs", "google-go", "kubeless-python", "kubeless-nodejs", "openwhisk-java-maven", "openwhisk-nodejs", "openwhisk-php", "openwhisk-python", "openwhisk-ruby", "openwhisk-swift", "spotinst-nodejs", "spotinst-python", "spotinst-ruby", "spotinst-java8", "plugin" and "hello-world"
    --template-url / -u ................ Template URL for the service. Supports: GitHub, BitBucket
    --template-path .................... Template local path for the service.
    --path / -p ........................ The path where the service should be created (e.g. --path my-service)
    --name / -n ........................ Name for the service. Overwrites the default name of the created service.
```

依照個人喜好選擇serverless language template : 這邊我選擇`nodejs` 
```sh
serverless create -t aws-nodejs --name serverless-demo
```

# Set aws config

可以透過`aws cli`或`serverless config credentials`來進行設定aws環境  
兩者皆會在你目前使用者目錄下建立 `~/.aws/credentials`
```sh
> aws configure
   AWS Access Key ID [****]:
   AWS Secret Access Key [****]:
   Default region name [us-west-1]: us-west-2
   Default output format [None]:
```

```sh
> serverless config credentials -k [your_iam_key] -s [your_iam_secret]
```

> [aws cli install](https://docs.aws.amazon.com/zh_tw/streams/latest/dev/kinesis-tutorial-cli-installation.html)

# Deploy 
將目前的nodejs程式發佈到aws環境  
自動將所需要的設定一次部屬完成   
若你有更新程式碼    
也是同樣用`deploy`指令進行部屬更新  
> deploy = delete old code + create new code
> aws部屬相關東西 : CloudFormation, Lambda, Cloudwatch, S3, IAM ...
```sh
serverless deploy -v
```

部屬完成後會得到下列訊息 
```sh
Serverless: Stack update finished...
Service Information
service: serverless-demo
stage: dev
region: us-east-1
stack: serverless-demo-dev
api keys:
  None
endpoints:
  None
functions:
  hello: serverless-demo-dev-hello
layers:
  None

Stack Outputs
HelloLambdaFunctionQualifiedArn: arn:aws:lambda:us-east-1:994755830156:function:serverless-demo-dev-hello:1
ServerlessDeploymentBucketName: serverless-demo-dev-serverlessdeploymentbucket-1rht7cwa1sw7l
```

# Test by inovke

測試你部屬的程式是否運作正常

Online test 
```sh
> serverless invoke -f hello -d '{"key3": "value3","key2": "value2","key1": "value1"}'
{
    "statusCode": 200,
    "body": "{\"message\":\"Go Serverless v1.0! Your function executed successfully!\",\"input\":\"{key3: value3,key2: value2,key1: value1}\"}"
}
```

Local test
```sh
> serverless invoke local -f hello -d '{"key3": "value3","key2": "value2","key1": "value1"}'
```



# Remove 

如果你覺得serverless專案不要了  
也可以透過`remove`方法一鍵移除環境  
```sh
> serverless remove
```

> 若沒有這個東西，傳統清理aws demo環境方法真的痛苦  
> 要一個一個來，還怕哪邊沒清乾淨然後產生莫名的費用  
> (lambda, s3, cloudwatch, cloudformation...)  


# Summary
想寫這篇文章想很久了  
剛好最近過年有空來研究一下  
一開始像無頭蒼蠅那樣找資料  
然後去AWS Lambda試老半天  
有弄出一些成果  
但部屬架設流程不是我想像中那麼順暢  
(AWS Console 部屬真的很痛苦...)  
後來得知serverless.com的工具後  
部屬設定變簡單了  
可以專注在核心程式上  
這才達到serverless核心開發理念  
其實現在有蠻多serverless部屬工具 :   
serverless, SAM, Up ...   
選擇適合自己的serverless工具可以事半功倍  



# Reference
 - [30天鐵人賽介紹 AWS 雲端世界 - 19:　改變雲端架構的核心計算服務 Lambda](https://ithelp.ithome.com.tw/articles/10193218)