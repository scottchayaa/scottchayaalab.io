---
title: 'EPS32 DEVKIT V1 Arduino 開發環境'
date: 2019-08-24T22:00:00+08:00
image: "/post/2019/08/24/cover.png"
description: 昨天手滑不小心把 eps8266 弄到燒壞，剛好手邊還有一顆沒用過的 EPS32 開發版，環境建置上比 8266 繁雜，不過好在還是有前人踩坑的智慧，讓我順利把開發版上的 Hello Led 範例弄好。 看圖就知道其實 eps32 的腳位非常豐富，另外還有支援 802.11b/g/n Wi-Fi 和 Bluetooth 4.2/BLE ，價格也蠻親民的，網路上買(露天、蝦皮)大約可以 NTD 200 以內，未來某一天希望可以將所有腳位玩過一遍。
tags: ["arduino", "eps32"]
---

# Pinout
![](/post/2019/08/24/1.png)

# 手動安裝 (舊版)
 - [Installing the ESP32 Core On – Windows OS](https://lastminuteengineers.com/esp32-arduino-ide-tutorial/#installing-the-esp32-core-on-windows-os)

因為當時沒找到 `Arduino board manager` (開發版管理員) 的安裝方式  
所以就先用舊的手動安裝方法 (之後會推下面的自動安裝)  
這邊整理以下手動安裝的重點  

 - 建立資料夾路徑 `My Documents > Arduino > hardware > espressif > esp32` ，下載解壓縮後的資料就放這邊
 - 執行 `get.exe` ，如果需要 `python` 的話就請自行安裝
 - 然後`重開` Arduino IDE，`工具 > 開發版 > 就會出現 esp32 的開發版工具 > 選擇 DOIT EPS32 DEVKIT V1` 

![](/post/2019/08/24/2.png)

# 自動安裝 (推薦)
 - [Installing the ESP32 Board in Arduino IDE (Windows, Mac OS X, Linux)](https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/)

 - 基本上就是把這段 `https://dl.espressif.com/dl/package_esp32_index.json`  
 - 加到你的 `Additional Boards Manager URLs` 裡面  
 - 到 `Tools > Board > Boards Manager…`
 - 搜尋 `ESP32` 然後安裝 `"ESP32 by Espressif Systems"`
 - 完成 > 選擇 `DOIT EPS32 DEVKIT V1`

# Run Blink Example
開發環境建置好後  
照慣例就先來做最基本的程式 : LED Blink  

```ino
int ledPin = 2; // 版子上就有的 led 藍燈

void setup()
{
  pinMode(ledPin, OUTPUT);
}

void loop()
{
  digitalWrite(ledPin, HIGH);
  delay(200);
  digitalWrite(ledPin, LOW);
  delay(100);
}
```

選擇`序列阜(Serial COM port)` 和 `工具 > Upload Speed : "115200"` 然後 `上傳程式` 
![](/post/2019/08/24/3.png)


![](/post/2019/08/24/4.png)

# [Troubleshooting – Booting up ESP32](https://lastminuteengineers.com/esp32-arduino-ide-tutorial/#troubleshooting-booting-up-esp32)

![](/post/2019/08/24/5.png)

注意：這邊有個 esp32 燒錄的坑  
就是出現 `Connecting..._____....` 的訊息時  
若有出現錯誤訊息 : `A fatal error occurred: Failed to connect to ESP32: Timed out waiting for packet header`  
記得按下 esp32 版子上的 `BOOT` 鍵  
然後他才會`真正開始`進行上傳程式  

> 好像按過 BOOT 鍵後，之後他就會自動偵測不用再一直按  
> 不過如果又出現 time out 的話就再按 BOOT 鍵吧  


# Summary
其實 Ardunio 還真的蠻好入門的  
程式邏輯不會太複雜  
可能我現在還在寫入門程式  
之後開始要做自走車、接串流影像、Wifi控制 ... 等
應該就會要寫很多複雜的 function 

我覺得對軟體工程師來說  
要碰硬體時會產生以下疑問 :  

 - 我要選什麼開發版  
 - 要怎麼重刷韌體
 - 這些 Pin 腳到底是做什麼
 - 要怎麼輸出訊號到電腦上
 - 韌體版本是有哪些/哪邊不同

總之，會有各種問題  
不過好在現在google 方便  
只要下好要找的版子型號關鍵字  
基本上就應該可以找到教學  

# Reference
 - [ESP32 with Arduino IDE](https://lastminuteengineers.com/esp32-arduino-ide-tutorial/)