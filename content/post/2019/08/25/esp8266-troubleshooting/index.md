---
title: 'How to remove Arduio IDE completely'
date: 2019-08-25T14:00:00+08:00
image: "/img/default/arduino.png"
description: esp8266 原本是可以順利 complie 的，昨天不知怎麼搞得突然變得不行，然後出現「未知的版子」、「編譯錯誤」，在網路上找也找不到通俗的錯誤，後來在別台電腦上裝 Arduino IDE 竟然可以順利 complie，最後在重裝 IDE 之下，終於順利讓原先的環境回復正常。
tags: ["arduino", "esp8266"]
---

# EPS8266 Compile Error : 未知的板子

![](/post/2019/08/25/1.png)


# Reinstall Step

- uninstall `Arduino` applcation

- Windows
  - remove `C:\Users\(username)\Documents\Arduino`
  - remove `C:\Users\(username)\AppData\Local\Arduino15`

- Mac OS
  - remove `/Users/(username)/Library/Arduino15`

> 然後重新再安裝 `Arduino IDE` 即可


重裝 IDE 和安裝開發工具後  
就沒再出現`未知的版子`錯誤了  



