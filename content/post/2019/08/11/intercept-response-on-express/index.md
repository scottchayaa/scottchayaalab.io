---
title: '如何擷取 Express response 訊息並做 log 紀錄'
date: 2019-08-11T23:30:00+08:00
image: "/img/default/nodejs.png"
description: 一般在使用 express body-parser 時，我們可以很輕鬆地擷取並解析 requset body 訊息，但今天如果要擷取 response body 的訊息呢？我發現中文資料好像不是很容易找到，但如果用英文搜尋 intercept response 的關鍵詞，可以很容易查到資料。 最近剛好有個練習是，想說把所有路由進來的 request 和 response 都存 log 下來，不過中間 response 在處理時卡關了一下，解完後覺得蠻有意思的，拿出來分享一下。
tags: ["nodejs"]
---

# [morgan-body](https://www.npmjs.com/package/morgan-body)
剛好也有人也跟我一樣有 request 和 response 的需求  
使用方法很簡單，而且他還有特別處理 json 訊息格式化  
基底也是使用 morgan 這個 middleware log  

快速使用
```js
const express = require('express');
const morganBody = require('morgan-body');
const uuid = require('node-uuid')

const app = express();
app.use(express.json()); // 取代 bodyParser.json()

// 定義 :id token, 因為他裡面有特別用來區分哪個 req, res 是屬於哪個 :id
// 當 request 流量大時才可以找得到正確對應的 request, response log
app.use(function (req, res, next) {
  req.id = uuid.v4()
  next()
});

// 自訂錯誤訊息: morgan-Body
morganBody(app);
```

**輸出結果**  
![](/post/2019/08/11/1.png)


> 雖然用別人的也不錯  
> 但工程師的性格還是希望可以用現有工具  
> 做出跟他差不多的功能  
> 把這個功能實作過程搞懂才覺得滿足哈  


# 工程師研究的 response 攔截版本

```js
// 攔截 response body 並暫存到 res.__body_response
app.use(function (req, res, next) {
  var originalSend = res.send;
  res.send = function (body) { // res.send() 和 res.json() 都會攔截到
    res.__body_response = body;
    originalSend.call(this, body);
  }
  next();
});

// 自訂錯誤訊息
app.use(morgan(function (tokens, req, res) {
  return [
    tokens.method(req, res),
    tokens.url(req, res),
    tokens.status(req, res),
    '-',
    tokens['response-time'](req, res),
    'ms',
    '\nrequest: ' + JSON.stringify(req.body),
    '\nresponse: ' + res.__body_response, // 利用剛剛暫存的 __body_response
  ].join(' ')
}));
```

**輸出結果**  

![](/post/2019/08/11/2.png)


> 自己 DIY 版本沒有 `req.id` 的問題  
> 不過建議還是要加入才會比較好追蹤 request/response LOG  
> 加入方法參考 : [use custom token formats](https://github.com/expressjs/morgan#use-custom-token-formats)  


# Summary
擷取 response 訊息的目的  
基本上是用來驗證 API 回傳是否正確  
其實本篇還可以再修改一下  
改成只遇到 4xx 和 5xx 時才會自錄 API Log (req/res)  
除非有特殊需求，不然全部記錄可是很吃空間  

另外也學到 [call](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Global_Objects/Function/call) 的用法  
原來攔截 response 是需要靠覆寫/串接原先 `res.send` 的方法  
攔截後存在 res 自己的物件裡面供後面的方法取用  
其實說不定還有其他更不錯存取 response 的方式  
有的話歡迎在下方↓↓↓留言推薦給我喔 ! 


# Reference

- [npm : morgan-body](https://www.npmjs.com/package/morgan-body)
