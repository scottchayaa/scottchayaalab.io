---
title: 'php 7 透過opcache提升執行效能'
date: 2019-01-13T22:00:00+08:00
image: "/post/2019/01/13/cover.png"
description: 在我自己的本機環境下測試，透過opcache優化後，php執行反應速度快4~5倍
tags: ["php", "docker", "laravel"]
---

# My enviriment
 - [Docker](https://docs.docker.com/docker-for-windows/install/) on Windows 10
 - [laradock : latest](http://laradock.io/)

# opcache config : old

laradock/php-fpm/opcache.ini
```ini
opcache.enable="1"
opcache.memory_consumption="256"
opcache.use_cwd="0"
opcache.max_file_size="0"
opcache.max_accelerated_files = 30000
opcache.validate_timestamps="1"
opcache.revalidate_freq="0"
```

> 這是laradoc預設的opcache設定，以下我們來做一些優化↓

# opcache config : new

整合官方的建議設定  
適合在一般開發或測試環境上的設定值  

> 但建議在local開發環境上還是將`opcache.revalidate_freq`改為`0`  
> 不然每次改完php code還要等60秒才可以更新有點煩...

laradock/php-fpm/opcache.ini
```ini
;OPcache打開/關閉開關，當設置為0時，會關閉Opcache, 代碼沒有被優化和緩存。
opcache.enable = 1

;OPcache共享內存存儲大小，用於存儲預編譯的opcode（以MB為單位）
opcache.memory_consumption = 256

;這是一個很有用的選項，但是似乎完全沒有文檔說明
;PHP使用了一種叫做字符串駐留（string interning）的技術來改善性能。
;例如，如果你在代碼中使用了1000次字符串"foobar"，在PHP內部只會在第一使用
;這個字符串的時候分配一個不可變的內存區域來存儲這個字符串，其他的999次使用
;都會直接指向這個內存區域。這個選項則會把這個特性提升一個層次——默認情況下這
;個不可變的內存區域只會存在於單個php-fpm的進程中，如果設置了這個選項，那麼它
;將會在所有的php-fpm進程中共享。在比較大的應用中，這可以非常有效地節約內存，
;提高應用的性能。
;這個選項的值是以兆字節（megabytes）作為單位，如果把它設置為16，
;則表示16MB，默認是4MB，這是一個比較低的值。
;(default "4")
opcache.interned_strings_buffer = 8

;這個選項用於控制內存中最多可以緩存多少個PHP文件。
;這個選項必須得設置得足夠大，大於你的項目中的所有PHP文件的總和。
;設置值取值範圍最小值是 200，最大值在 PHP 5.5.6 之前是 100000，
;PHP 5.5.6 及之後是 1000000。也就是說在200到1000000之間。
;(default "2000")
opcache.max_accelerated_files = 4000

;如果啟用，OPcache會在 opcache.revalidate_freq 設置的秒數去檢測文件的timestamp
;檢查腳本是否更新。
;建議在開發/測試環境設為"1"，而正式環境設為"0"
;(default "1")
opcache.validate_timestamps = 1

;這個選項用於設置緩存的過期時間（單位是秒），
;當這個時間達到後，opcache會檢查你的代碼是否改變，
;如果改變了PHP會重新編譯它，生成新的opcode，並且更新緩存。
;值為"0"表示每次請求都會檢查你的PHP代碼是否更新（這意味著會
;增加很多次stat系統調用，譯註：stat系統調用是讀取文件的狀態，
;這裡主要是獲取最近修改時間，這個系統調用會發生磁盤I/O，
;所以必然會消耗一些CPU時間，當然系統調用本身也會消耗一些CPU時間）。
;建議可以在開發環境中把它設置為"0"，正式環境下不用管。
;(default "2")
opcache.revalidate_freq = 60

;如果啟用，則會使用快速停止續發事件。所謂快速停止續發事件是指依賴
;Zend 引擎的內存管理模塊 一次釋放全部請求變量的內存，而不是依次釋放
;每一個已分配的內存塊。
;該指令已在PHP 7.2.0中被刪除。快速關機序列的一個變種已經被集成到PHP中，並且如果可能的話將被自動使用。
;(default "0")
opcache.fast_shutdown = 1 

;CLI環境下，PHP啟用OPcache。這主要是為了測試和調試。從 PHP 7.1.2 開始，默認啟用。
opcache.enable_cli = 1

;如果啟用，OPcache將在哈希表的腳本鍵之後附加改腳本的工作目錄，
;以避免同名腳本衝突的問題。禁用此選項可以提高性能，但是可能會導致應用崩潰
;(default "1")
opcache.use_cwd = 0

opcache.max_file_size = 0
```

> `items description`  
參考 : [opcache configuration](http://php.net/manual/zh/opcache.configuration.php#ini.opcache.revalidate-freq)

# opcache config : production

當如果要發佈到正式環境時  
建議將`validate_timestamps`設為 `0`  
因為你上道正式環境後  
基本上會將`php-fpm`, `ngnix`, `php-worker`的container服務重啟  
不會再動到php code  
所以將`validate_timestamps`設為 `0`後  
系統就會將php opcode存到cache  
所以之後http request進來就直接讀opcache裡面的code
反應速度將會最大化  

```ini
opcache.validate_timestamps = 0
```

# Summary

以前或多或少會聽說php效能不好什麼的  
但沒有實際真正了解過  
後來得知`opcode`, `opcache`關鍵詞後  
研究後發現透過opcache的確可以加速伺服器的處理回應  

伺服器搭配laradock, 再加上opcache設定  
擺脫php執行效能不好的謠言


# Reference

- [Opcode是啥以及如何使用好Opcache](https://www.zybuluo.com/phper/note/1016714)
- [PHP7有沒有你們説的那麼牛逼(php7.1 和 php5.6 橫向對比)](https://hk.saowen.com/a/4e7aab5fdda567dc3545d850b3808c664ef8a0bc51bd91e460de7d2c6e5bf097)
- [使用 Zend Opcache 加速 PHP](https://cnzhx.net/blog/zendopcache-accelerate-php/)
- [10年漫長等待，PHP 7終於問世](https://www.ithome.com.tw/news/101599)