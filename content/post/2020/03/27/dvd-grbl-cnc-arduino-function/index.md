---
title: 'DVD 雷射雕刻機 : 功能篇'
date: 2020-03-27T18:00:00+08:00
image: "/post/2020/03/27/dvd-grbl-cnc-arduino-function/cover.png"
description: 起因是看了一篇使用不要的光碟機打造雷射雕刻機，開啟了接下來的不歸路
draft: false
tags: ["grbl","arduino","cnc"]
---

起因 :
https://www.instructables.com/id/Pocket-laser-engraver/


這邊首先先處理功能模組篇  
最主要的目標是 :  
把 DVD 裡面的步進馬達區塊拆下來後  
連接 Arduino + CNC 模組板  
並測試運轉是否正常  

然後 Z 軸的控制還有分控制 「伺服馬達 (Servo)」 和 「雷射模組 (Laser)」  
後面我們也會介紹  

# 材料準備

 - 二手光碟機 * 2
 - Arduino UNO * 1
 - CNC Shield v3 模組 * 1
 - A4988 步進電機驅動器 * 2
 - 電壓器 (9v ~ 12v) * 1
 - 跳線帽 * 12
 - 伺服馬達 * 1
 - 柔性扁平排線 4 pins * 2


# Step 1. 拆光碟機

![](/post/2020/03/27/1.png)

想辦法取出步進馬達區塊 x 2  

- 拆解參考 : 
  - https://www.youtube.com/watch?v=TbL68UXbq1k
  - https://www.youtube.com/watch?v=FtGEMv4pAJc

`心得` : 讀片夾要先抽出來，接下來才會比較好拆，讀片夾可以用針磋出來


# Step 2. 焊接步進馬達控制排線

![](/post/2020/03/27/2.png)

![](/post/2020/03/27/3.png)

原來的軟排線可以剪掉  

![](/post/2020/03/27/4.png)

話說怎麼知道馬達的 4 個焊點哪 2 點是一組 ?  
可以使用三用電表，轉到會「嗶嗶叫」的檔位進行測試  
測到 2 點會嗶的表示通電 = 是一組的  
 
焊接技巧純屬經驗  
如果不熟焊接的話建議先隨便拿個 PCB 版  
然後剪幾段短銅線在一旁練習  
練到手感差不多抓到訣竅後再來挑戰  

`小訣竅` : 在要焊接的接點上`先上一些焊錫`，會比較好焊  


# Step 3. Arduino Uno 和 CNC Shield V3 組裝

![](/post/2020/03/27/5.png)

1. 接上 A4988 晶片  
`特別注意` : 插上 9V~ 12V 電源後，如果 A4988 晶片接反的話可是會直接`「燒掉」`(我就直接壞了一顆)  
其他參考 : [步進馬達 & CNC shield & A4988 (介紹)](https://makeryan.wordpress.com/2017/11/27/micromouse-%E6%AD%A5%E9%80%B2%E9%A6%AC%E9%81%94-cnc-shield-a4988-%E4%BB%8B%E7%B4%B9/)
2. 接上 Step 2 焊好的步進馬達排線，接反沒關係，只會移動反方向
3. 接上跳線帽 (3 * 4)，接上與沒接的差別在馬達轉動的精度，全接上後會比較穩定
4. 接上 9v ~ 12v 的電源供應器，我這邊直接買`可調變壓器`(3v ~ 12v)，另外焊接電源孔母座(大小要注意)
5. 下面會先用伺服馬達作為 z 軸演示，可以先照著上圖這樣配置，馬達的紅棕線為正負極接到 5v, GND，橘線為 PWM 控制腳位接到 Z+

# Step 4. 燒錄 grbl-servo

1. Download grbl-servo zip : https://github.com/robottini/grbl-servo  

2. 載入剛剛下載的函式庫，載入成功後會看到 `grbl-servo-master`
![](/post/2020/03/27/7.png)

3. 點選 `grbl-servo-master` 就會把函示 include 到檔案裡  
![](/post/2020/03/27/8.png)

4. 確認連接好 Arduino 開發版  
![](/post/2020/03/27/6.png)

5. 開始燒入  
![](/post/2020/03/27/9.png)


# Step 5. 製作 Gcode 檔

G 代碼是連接電腦和3d印表機的「橋樑」，利用G代碼，我們可以通過電腦「告訴」3d印表機什麼時候列印，在哪兒列印，如何移動，擠出多少等。

1. 這邊我們使用 [Inkscape](https://inkscape.org/zh-hant/) 進行製圖和轉檔  

2. 另外還要下載轉 Gcode for CNC Shield 的 plugin : [Inkscape Laser Plug-In](https://jtechphotonics.com/?page_id=2012)

3. 如何匯入 `Inkscape Laser Plug-In`
![](/post/2020/03/27/10.png)  
將上述下載的檔案都放到 `C:\Program Files\Inkscape\share\extensions`

4. 重開 Inkscape > 擴充功能 就可以看到了
![](/post/2020/03/27/11.png)  

5. 檔案 > 文件屬性 : 將畫布尺寸設定為 17 x 17 mm , 畫布大小請依照各自情況而定
![](/post/2020/03/27/12.png)  

6. 在畫布上隨便寫個文字吧
![](/post/2020/03/27/13.png)  

7. 轉 Gcode 之前，記得一定要將圖片物件轉換成`路徑`格式
![](/post/2020/03/27/14.png)  

8. 匯出設定參數，可以參考下圖  
Laser Power S# : 用在 Servo 裡的話表示轉動角度，這裡我是設定 90 度
![](/post/2020/03/27/15.png)  


`套用`後就會成功匯出 Gcode 檔了

9. 匯出成功後你會得到一個爆炸圖，別擔心這樣是正常的
![](/post/2020/03/27/16.png)  



# Step 6. Using Grbl 

Grbl Controller 3.6 : [Download](https://grbl-controller.software.informer.com/3.6/)

其他 GUI 參考 : 
https://github.com/grbl/grbl/wiki/Using-Grbl


![](/post/2020/03/27/17.png)

1. 選擇已燒錄成功的 Arduino COM Port
2. Baud Rate : 115200
3. 選擇你剛剛畫好產生的 Gcode 檔 > Begin
4. 顯示目前執行到哪個地方

# 實機完成演示

{{< youtube oj-pbBEeVrM >}}


# Summary

看別人實作的影片都感覺很簡單  
但實際動手做的時真是遇到滿滿的坑  
特別是選 FW 時，其實還有區分  
因為要使用伺服馬達當作 Y 軸  
所以特別找到有 grbl 支援 servo 的版本  
之前實作一直不知道 servo 怎麼驅動  
只有 X, Y 軸會動  

Inkspace 也是一個坑  
網路上很多資料各種做法  
後來才發現原來 Larser plugin extention 也可以支援驅動 servo 轉動角度  
因為控制命令是一樣的 : M03 S## (開: 幾度), M05(關)  

至於 grbl 的控制命令  
這部分我還不是很懂怎麼配置  
有空研究完成再陸續補上  

然後看到最後的人會不會有個疑問...  
雷射頭在哪 ?  
抱歉，買的時候買到功率比較小的  
所以就改成用伺服馬達當作替代測試  
雷射功率至少要 100 mw 以上好像才可以達到燒焦的效果  
我後來發現我買的是 5V, 5mw....
之後應該會買 250mw  


# Reference
 - https://www.youtube.com/watch?v=Gm6bH3p6cNQ&list=PLcFE2K6uLREfsOshfInsi4wgHPNdfsxKK&index=2&t=146s
 - https://www.youtube.com/watch?v=Q5ma1HDuotk&list=PLcFE2K6uLREfsOshfInsi4wgHPNdfsxKK&index=3&t=7s
 - https://www.youtube.com/watch?v=vxhYIVOXXXk&list=PLcFE2K6uLREfsOshfInsi4wgHPNdfsxKK&index=6&t=369s
 - https://www.youtube.com/watch?v=SamSyZtsA5E&list=PLcFE2K6uLREfsOshfInsi4wgHPNdfsxKK&index=7&t=0s
 - https://www.youtube.com/watch?v=SamSyZtsA5E&list=PLcFE2K6uLREfsOshfInsi4wgHPNdfsxKK&index=7&t=0s
 - https://www.youtube.com/watch?v=XKnEcPSUlLQ&list=PLcFE2K6uLREfsOshfInsi4wgHPNdfsxKK&index=8&t=257s
 - [Inkscape : 向量圖檔轉G-code文字教學](http://www.fpic.com.tw/muherz/tw/file_convert_text.html)
 - [Drawing Robot - Arduino Uno + CNC Shield + GRBL](https://www.thingiverse.com/thing:2349232)
 - [GRBL CNC Shield + Z Axis servo MIGRBL](https://electricdiylab.com/grbl-cnc-shield-z-axis-servo-migrbl/)
 - [Cyclone PCB 使用Inkscape Gcodetools 將圖片轉換G碼(G-CODE)](https://engineer2designer.blogspot.com/2017/06/cyclone-pcb-inkscape-gcodetools-gg-code.html)