---
title: "About"
date: 2018-09-15T20:00:00+08:00
lastmod: 2018-09-15T20:00:00+08:00
menu: "main"
weight: 50
comment: false
mathjax: false
---

![](https://s.gravatar.com/avatar/51596d580161f4cdcfddb7a893eecc60?s=200)

喜歡挑戰高難度遊戲，尋找屬於自己的攻略方法  
運動時喜歡聽 podcast ，拿著啞鈴兩眼放空思考人生  
快速迭代產品功能，重構系統優化效能  
嘗試運用合理運算資源和架構解決各種難題  
目前熱衷於建置 Serverless 服務和 CICD 流程  

> Think big, do small, act fast.

# Badges
[![](/img/badges/aws_certified_cloud_practitioner.png)](https://www.certmetrics.com/amazon/public/badge.aspx?i=9&t=c&d=2018-12-11&ci=AWS00690083&dm=80)
 

# Skills
- Frontend
  - html5, Bootstrap, javascript, es6
- Backend
  - PHP, Nodejs
  - Storage : Mysql, Redis
  - Authentication : OAuth, JWT
  - MVC : Laravel, Express
- DevOps
  - Linux, AWS, GCP, Docker, GitLab CI/CD  
- IOT
  - Raspberry pi, Linkit 7688

# Experiences 

- **2019**
  - LINE聊天機器人
      - 自製 喵神 line bot : [add bot link](https://line.me/R/ti/p/B9tZrC59os)
      - LINE Message API 系統整合：客製化訊息推播、報名報到、優惠券推播...等

- **2018**
  - 建立SPA API開發流程模式，前後端工作分離，提升協作效率，並導入JWT安全機制
  - 導入[GitLab CI/CD](https://docs.gitlab.com/ee/ci/) 流程，降低開發人員部屬環境時的操作錯誤，提升線上系統品質
  - 持續優化與測試佈署每個Git專案

- **2017**
  - Laravel 5 開發購物網站系統，整合[ECPay](https://www.ecpay.com.tw/)金流和物流功能 
  - 宏誠官網 : [fairpower.com.tw](http://fairpower.com.tw) ([show](/img/projects/2017-fairpower.png))
  - IU產品研發
      - 台灣版官網 : [i-u.com.tw](http://i-u.com.tw) ([show](/img/projects/2017-IU.png))
      - Makuake日本募資 : [www.makuake.com/project/i-u/](https://www.makuake.com/project/i-u/) ([show](/img/projects/2018-IU-makuake.com.png))
      - API Service 規劃與開發，整合OAuth2.0安全機制

- **2016**
  - 新光長照 : [care.skl.com.tw](http://care.skl.com.tw) ([show](/img/projects/2016-SKL.png))
  - iPex : [www.ipex-live.com](http://www.ipex-live.com) ([show](/img/projects/2016-IPEX.png))
  - 體勢釋放 : [mybetterlife.com.tw](http://mybetterlife.com.tw) ([show](/img/projects/2016-mybetterlife.png))
  - 新光長照 : HTML5遊戲開發(Phaser game framework) ([show](/img/projects/2016-SKL-GAME.png))
  - 參與 : 台北市警察局無線電系統汰換專案, 翡翠水庫無線電系統汰換專案 

- **2013-2015**
  - 研發[HLS](https://zh.wikipedia.org/wiki/HTTP_Live_Streaming)撥放器，提供線上影音串流解決方案 : HTML5, HLS decode, Streaming server, FLASH
  - NAS NVR 產品研發 : arm linux, 錄影程式, lighttpd server
  - 整合海康DVR產品，客製化Web播放器 (OCX)

- **2012**
  - 論文研究，[應用Google N-gram 與 PTFICF的自動文件分類系統](https://hdl.handle.net/11296/uk8ad8)，改進分類演算法精準度

- **2008**
  - 微軟學生大使，校園推廣微軟技術

# Laravel Packagist
 - [scottchayaa/allpay](https://packagist.org/packages/scottchayaa/allpay#1.0)

# NPM 
 - [Scokit](https://www.npmjs.com/package/scokit)