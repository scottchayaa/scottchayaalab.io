---
title: "一些不錯的技術文章"
date: 2020-01-13T14:00:00+08:00
lastmod: 2020-01-13T14:00:00+08:00
comment: false
mathjax: false

menu:
  main:
    parent: "docs"
    weight: 3
---


# 2019
 - [[心得] PressPlay從AWS搬家到GCP一年的心得 - 看板 Soft_Job - 批踢踢實業坊](https://www.ptt.cc/bbs/Soft_Job/M.1562948175.A.FB1.html)
 - [进程与线程的一个简单解释 - 阮一峰的网络日志](http://www.ruanyifeng.com/blog/2013/04/processes_and_threads.html)