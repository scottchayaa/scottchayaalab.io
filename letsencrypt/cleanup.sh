#!/bin/sh
git rm $CI_PROJECT_DIR/static/.well-known/acme-challenge/$CERTBOT_TOKEN
git commit -m "GitLab runner - Removed certbot challenge file"
git push https://$GITLAB_USER_LOGIN:$GITLAB_API_TOKEN@gitlab.com/$CI_PROJECT_PATH.git HEAD:master
exit 0