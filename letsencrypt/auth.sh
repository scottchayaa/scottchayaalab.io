#!/bin/sh
mkdir -p $CI_PROJECT_DIR/static/.well-known/acme-challenge
echo $CERTBOT_VALIDATION > $CI_PROJECT_DIR/static/.well-known/acme-challenge/$CERTBOT_TOKEN
git add $CI_PROJECT_DIR/static/.well-known/acme-challenge/$CERTBOT_TOKEN
git commit -m "GitLab runner - Add certbot challenge file for certificate renew"
git push https://$GITLAB_USER_LOGIN:$GITLAB_API_TOKEN@gitlab.com/$CI_PROJECT_PATH.git HEAD:master

# 卡在這個迴圈，等待上面commit後所產生的 gitlab page pipeline 工作
# gitlab page pipeline 工作將會產生 https://$DOMAIN/.well-known/acme-challenge/$CERTBOT_TOKEN 這個頁面
# 如果在 10 * 30 (sec) 內成功的話，此script將會回 exit 0
# 如果失敗的話，此script將會回 exit 1
interval_sec=10
max_tries=30
n_tries=0
while [ $n_tries -le $max_tries ]
do
  status_code=$(curl -s -o /dev/null -I -w "%{http_code}" https://$DOMAIN/.well-known/acme-challenge/$CERTBOT_TOKEN)
  if [[ $status_code -eq 200 ]]; then
    echo $status_code
    exit 0
  fi
  n_tries=$((n_tries+1))
  echo "try : " $n_tries
  sleep $interval_sec
done
exit 1 