#!/bin/sh
end_epoch=$(date -d "$(echo | openssl s_client -connect $DOMAIN:443 -servername $DOMAIN 2>/dev/null | openssl x509 -enddate -noout | cut -d'=' -f2)" "+%s")
current_epoch=$(date "+%s")
days_diff=$((($end_epoch - $current_epoch) / 60 / 60 / 24))
if [ $days_diff -lt $RENEW_DAYS_THRESHOLD ]; then
    echo "============================"
    echo "Certificate is $days_diff days old, renewing now."
    echo "============================"
    certbot certonly \
    --preferred-challenges http \
    --manual \
    --agree-tos \
    --eff-email \
    -m "$GITLAB_USER_EMAIL" \
    -d "$DOMAIN" \
    --manual-public-ip-logging-ok \
    --manual-auth-hook ./letsencrypt/auth.sh \
    --manual-cleanup-hook ./letsencrypt/cleanup.sh
    echo "============================"
    echo "Certbot finished. Updating GitLab Pages domains."
    echo "============================"
    ls -la /etc/letsencrypt/live/$DOMAIN/
    curl --request PUT --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" --form "certificate=@/etc/letsencrypt/live/$DOMAIN/fullchain.pem" --form "key=@/etc/letsencrypt/live/$DOMAIN/privkey.pem" https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pages/domains/$DOMAIN
else
    echo "============================"
    echo "Certificate still valid for $days_diff days, no renewal required."
    echo "============================"
fi